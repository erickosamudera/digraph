const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const path = require('path');

module.exports = {
    entry: ["@babel/polyfill", "./digraph/src/renderer.tsx"],
    mode: 'development',
    devtool: 'source-map',
    output: {
        path: path.join(__dirname, './build'),
        filename: 'renderer.js',
        library: "renderer",
        libraryTarget: "umd"
    },
    module: {

        rules: [
          {
            test: /\.html$/,
            use: ['file?name=[name].[ext]'],
          },
          {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: { 
                loader: 'babel-loader',
                options: {
                    plugins: ["@babel/plugin-proposal-class-properties"], 
                    presets: ["@babel/preset-env", "@babel/preset-typescript", "@babel/preset-react"]
                }
            }
          },
          {
            test: /\.(ts|tsx)$/,
            exclude: /node_modules/,
            use: { 
                loader: 'babel-loader',
                options: {
                    plugins: ["@babel/plugin-proposal-class-properties"], 
                    presets: ["@babel/preset-env", "@babel/preset-typescript", "@babel/preset-react"]
                }
            }
          },
          {
            test: /\.css$/,
            use: [ 'style-loader', 'css-loader' ]
          }
        ],
    },
    target: 'node',
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    plugins: [
        new MonacoWebpackPlugin({
            languages: []
        })
    ]
};