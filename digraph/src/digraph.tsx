import React from 'react';
import ReactDOM from "react-dom";
import ProjectInitialiser from "./operation/project/ProjectInitialiser";
import FileManager from "./operation/project/FileManager";
import DigraphFileTree from "./ui/filetree/DigraphFileTree";
import FileIndexManager from './operation/project/FileIndexManager';
import VariableManager from './operation/project/VariableManager';
import ScenarioManager from './operation/project/ScenarioManager';
import DigraphCytoscape from './ui/cytoscape/DigraphCytoscape'
import log from 'loglevel';
import {DigraphVariableOperation} from './entity/DigraphVariable';
import {DigraphDOMContract} from './contract/DigraphDOMContract';
import CharacterManager from './operation/project/CharacterManager';
import SetVariableNodeHandler from './ui/cytoscape/SetVariableNodeHandler';
import StoryNodeHandler from './ui/cytoscape/StoryNodeHandler';
import BranchNodeHandler from './ui/cytoscape/BranchNodeHandler';
import ConnectorNodeHandler from './ui/cytoscape/ConnectorNodeHandler';
import CodexManager from './operation/project/CodexManager';
import EndingNodeHandler from './ui/cytoscape/EndingNodeHandler';
import RunningStateManager from './operation/project/RunningStateManager';
import {handleSuggestion} from './operation/suggestion/Suggestion';
import {readJsonAsync, writeJsonAsync} from './operation/file/FileIO';
import {Project} from './entity/Project';
import {handleStoryMonaco} from './operation/monaco/StoryMonaco';
import SceneManager from './operation/project/SceneManager';

export const startDigraph = function(
    projectData: Project,
    projectDir: string,
) : Promise<void> {
    const fileManager = new FileManager(projectDir);
    const fileTreeDOM = document.querySelector(DigraphDOMContract.TREE_CONTAINER);

    const handleInitialise = async (fileList: {[key: string]: object}) => {
        const writePromises = Object.keys(fileList).map(async (fileName) : Promise<void> => {
            return fileManager.checkIfFileExists(fileName)
                .then((isFileExists) => {
                    if (! isFileExists) {
                        const fileData = fileList[fileName];
                        const logAbsoluteFilePath = fileManager.getAbsolutePath(fileName);
                        log.info(logAbsoluteFilePath, "doesn't exist, creating new", logAbsoluteFilePath, "file");
                        return fileManager.addNewFile(fileName, fileData);
                    } else {
                        log.info(fileName, "already exists, moving on...");
                        return Promise.resolve();
                    }
                });
        });
        return Promise.all(writePromises).then(() => Promise.resolve());
    };

    const handleSave = async (fileList: {[key: string]: object}) => {
        const writePromises = Object.keys(fileList).map((fileName) => {
            const filePath = fileManager.getAbsolutePath(fileName);
            LOGGER.info("Saving data to file:", filePath);
            const fileData = fileList[fileName];
            return writeJsonAsync(filePath, fileData);
        });
        return Promise.all(writePromises).then(() => Promise.resolve())
    };

    const handleLoad = async (fileName: string) : Promise<any> => {
        const filePath = fileManager.getAbsolutePath(fileName);
        LOGGER.info("Loading file from:", filePath);
        return readJsonAsync(filePath)
            .catch(
                (err) => {
                    LOGGER.error("Error loading digraph file", err);
                    return {};
                }
            );
    };

    const fileIndexManager = new FileIndexManager(handleInitialise, handleSave, handleLoad);
    const variableManager = new VariableManager(handleInitialise, handleSave, handleLoad);
    const runningStateManager = new RunningStateManager(handleInitialise, handleSave, handleLoad);
    const characterManager = new CharacterManager(handleLoad);
    const sceneManager = new SceneManager(handleInitialise, handleLoad);
    const codexManager = new CodexManager(handleInitialise, handleLoad);
    const scenarioManager = new ScenarioManager(
        handleInitialise,
        fileManager.addNewDirectory,
        fileManager.addNewFile,
        fileManager.deleteFile,
        fileIndexManager.registerNewFileToIndex,
        fileIndexManager.getFileName,
        fileIndexManager.deleteFromIndex
    );

    const handleListAllVariables = function() {
        return variableManager.listAllVariables();
    }

    const handleSetVariable = function(variableId: string, setterId: string, description: string, operation: DigraphVariableOperation, value: number) {
        return variableManager.setVariable(variableId, setterId, description, operation, value);
    }

    const handleUnsetVariable = function(variableId: string, setterId: string) {
        return variableManager.unsetVariable(variableId, setterId);
    }

    const handleNewScenario = async function(startNodeId: string, newScenarioName: string) : Promise<string> {
        return scenarioManager.addNewScenarioAsync(startNodeId, newScenarioName)
            .then(
                newNodeId => {
                    refreshTree();
                    return newNodeId;
                }
            );
    }

    const handleDeleteScenario = async function(targetNodeId: string) : Promise<void> {
        return scenarioManager.deleteScenarioAsync(targetNodeId)
            .then(() => { refreshTree(); });
    }

    const handleGotoScenario = async function(scenarioNodeId: string) : Promise<void> {
        fileIndexManager.getFileName(scenarioNodeId)
            .then(fileName => {
                const absolutePath = fileManager.getAbsolutePath(fileName);
                LOGGER.info("Opening scenario file", absolutePath);
                return loadDigraphFile(absolutePath);
            });
    };

    const prepareSaveCytoscapeHandler = function(openDigraphFile: string) {
        return async function handleSaveCytoscapeData(cytocore: cytoscape.Core) : Promise<void> {
            LOGGER.info("Saving cytoscape data to", openDigraphFile);
            const digraphElements = cytocore.elements("[digraph=1]");
            return writeJsonAsync(openDigraphFile, digraphElements.jsons())
        }
    };

    const loadDigraphFile = async function(absoluteFilePath: string) {
        return readJsonAsync(absoluteFilePath)
            .then(
                (cytoscapeData) => {
                    const handleSave = prepareSaveCytoscapeHandler(absoluteFilePath);
                    const setVariableNodeHandlerFactory = function(cytoscapeInstance: cytoscape.Core) {
                        return new SetVariableNodeHandler(
                            cytoscapeInstance,
                            handleListAllVariables,
                            handleSetVariable,
                            handleUnsetVariable,
                            handleSave
                        )
                    };

                    const suggestionHandler = handleSuggestion(
                        projectData.gameplayLanguage, characterManager, codexManager, variableManager, sceneManager);
                    const storyMonacoHandler = handleStoryMonaco();
                    
                    const storyNodeHandlerFactory = function(cytocore: cytoscape.Core) {
                        return new StoryNodeHandler(
                            cytocore,
                            suggestionHandler,
                            storyMonacoHandler,
                            handleSave
                        );
                    };

                    const branchNodeHandlerFactory = function(cytocore: cytoscape.Core) {
                        return new BranchNodeHandler(
                            cytocore,
                            handleSave,
                            handleListAllVariables
                        )
                    };

                    const connectorNodeHandlerFactory = function(cytocore: cytoscape.Core) {
                        return new ConnectorNodeHandler(
                            cytocore,
                            handleSave,
                            handleGotoScenario,
                            handleDeleteScenario,
                            handleNewScenario
                        )
                    };

                    const endingNodeHandlerFactory = function(cytocore: cytoscape.Core) {
                        return new EndingNodeHandler( cytocore, handleSave );
                    }

                    ReactDOM.render(
                        <DigraphCytoscape
                            setVariableNodeHandlerFactory={setVariableNodeHandlerFactory}
                            storyNodeHandlerFactory={storyNodeHandlerFactory}
                            branchNodeHandlerFactory={branchNodeHandlerFactory}
                            connectorNodeHandlerFactory={connectorNodeHandlerFactory}
                            endingNodeHandlerFactory={endingNodeHandlerFactory}
                            handleSaveCytoscapeData={handleSave}
                            jsonData={cytoscapeData}
                        />,
                        document.querySelector(DigraphDOMContract.DIGRAPH_CONTAINER)
                    );

                    fileManager.getRelativePath(absoluteFilePath)
                        .onSuccess(relativePath => runningStateManager.updateLastReadFile(relativePath));
                }
            )
    };

    const refreshTree = function() {
        LOGGER.info("Refreshing file tree");
        ReactDOM.render(
            <DigraphFileTree
                onOpenFile={function(filePath: string) {
                    loadDigraphFile(filePath)
                        .catch((err) => LOGGER.error("Failed to open file", err));
                }}
                treeData={fileManager.fetchCurrentFileTree()}
            />,
            fileTreeDOM
        );
    };

    const initialiser = new ProjectInitialiser(
        variableManager,
        fileIndexManager,
        scenarioManager,
        characterManager,
        codexManager,
        runningStateManager,
        sceneManager
    );

    return initialiser.execute()
        .then(() => runningStateManager.getLastReadFile())
        .then(lastReadFile => { loadDigraphFile(fileManager.getAbsolutePath(lastReadFile)) } )
        .then(() => { refreshTree() } )
};

const LOGGER = log.getLogger("DigraphApplication");
