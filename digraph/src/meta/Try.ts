export default abstract class Try<T> {
    static failure<T>(error: Error) : Failure<T> {
        return new Failure(error);
    }

    static success<T>(data: T) : Success<T> {
        return new Success(data);
    }

    abstract get() : T;
    abstract getError() : Error;
    abstract isFailure() : boolean;
    abstract isSuccess() : boolean;

    onSuccess(callback: (data: T) => void) : Try<T> {
        if (this.isSuccess()) {
            callback(this.get());
        }
        return this;
    }

    onFailure(callback: (data: Error) => void) : Try<T> {
        if (this.isFailure()) {
            callback(this.getError());
        }
        return this;
    }

    getOrElseGet(callback: (err: Error) => T) : T {
        if (this.isFailure()) {
            return callback(this.getError());
        } else {
            return this.get();
        }
    }
    
    map<U>(callback: (data: T) => U) : Try<U> {
        if (this.isFailure()) {
            return Try.failure(this.getError());
        } else {
            try {
                const callbackResult = callback(this.get());
                return Try.success(callbackResult);
            } catch (err) {
                return Try.failure(err);
            }
        }
    }

    recover(callback: (err: Error) => T) : Try<T> {
        if (this.isFailure()) {
            try {
                const callbackResult = callback(this.getError());
                return Try.success(callbackResult);
            } catch (err) {
                return Try.failure(err);
            }
        }
        return this;
    }
}

class Success<T> extends Try<T> {
    private successValue: T;
    constructor(value: T) {
        super();
        this.successValue = value;
    }

    get() : T {
        return this.successValue;
    }

    getError() : Error {
        throw Error("invoking getError on success state.")
    }

    isFailure() : boolean {
        return false;
    }

    isSuccess() : boolean {
        return true;
    }
}

class Failure<T> extends Try<T> {
    private failureError: Error;
    constructor(error: Error) {
        super();
        this.failureError = error;
    }

    get() : T {
        throw this.failureError;
    }

    getError() : Error {
        return this.failureError;
    }

    isFailure() : boolean {
        return true;
    }

    isSuccess() : boolean {
        return false;
    }

}