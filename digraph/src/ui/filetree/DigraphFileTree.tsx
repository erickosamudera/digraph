import React from 'react';
import { Treebeard, decorators } from 'react-treebeard';

const filters = require('../../operation/DigraphFileTreeFilters');

const headerOverride = function({style, node})  {
    const fileType = node.children ? 'directory' : 'file';
    const iconType = node.children ? 'folder' : 'file-text';
    const iconClass = `fa fa-${iconType}`;
    const iconStyle = {marginRight: '5px'};

    const contextMenuTriggerAttr = {
        filepath: node.path,
        nodetype: fileType
    }

    return (
        <div style={style.base}>
            <div style={style.title}>
                <i className={iconClass} style={iconStyle}/>
                {node.name}
            </div>
        </div>
    );
};

decorators.Header = headerOverride;

interface FileTreeProps {
    treeData: JSON,
    onOpenFile: Function
}

interface FileTreeState {
    filterKeyword: string,
    filteredKeywords: any,
    cursor: any
}

const treeStyle = {
    tree: {
        base: {
            listStyle: 'none',
            // backgroundColor: '#21252B',
            margin: 0,
            padding: '8px',
            // color: '#9DA5AB',
            color: '#222',
            fontFamily: 'lucida grande ,tahoma,verdana,arial,sans-serif',
            fontSize: '14px'
        },
        node: {
            base: {
                position: 'relative',
                cursor: 'pointer'
            },
            link: {
                cursor: 'pointer',
                position: 'relative',
                padding: '0px 5px',
                display: 'block'
            },
            activeLink: {
                background: '#31363F'
            },
            toggle: {
                base: {
                    position: 'relative',
                    display: 'inline-block',
                    verticalAlign: 'top',
                    height: '22px',
                    width: '10px'
                },
                wrapper: {
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    margin: '-7px 0 0 -6px',
                    height: '10px'
                },
                height: 10,
                width: 10,
                arrow: {
                    fill: '#777',
                    // fill: '#9DA5AB',
                    strokeWidth: 0
                }
            },
            header: {
                base: {
                    display: 'inline-block',
                    verticalAlign: 'top',
                    // color: '#9DA5AB'
                    color: '#EEEEEE'
                },
                connector: {
                    width: '2px',
                    height: '12px',
                    borderLeft: 'solid 2px black',
                    borderBottom: 'solid 2px black',
                    position: 'absolute',
                    top: '0px',
                    left: '-21px'
                },
                title: {
                    lineHeight: '24px',
                    verticalAlign: 'middle'
                }
            },
            subtree: {
                listStyle: 'none',
                paddingLeft: '19px'
            },
            loading: {
                color: '#E2C089'
            }
        }
    }
};

//TODO: activate active file!

export default class DigraphFileTree extends React.Component<FileTreeProps, FileTreeState> {
    constructor(props : FileTreeProps) {
        super(props);
        this.state = { filterKeyword: "", filteredKeywords: null, cursor: null }
        this.onToggle = this.onToggle.bind(this);
        this.switchActiveNode = this.switchActiveNode.bind(this);
        this.onFilterKeyUp = this.onFilterKeyUp.bind(this);
    }

    private onToggle(node, toggled) {
        this.switchActiveNode(node);

        if (node.children) {
            node.toggled = toggled;
        }

        if (!node.children) {
            this.props.onOpenFile(node.path);
        }
    }

    private switchActiveNode(node) {
        const cursor = this.state.cursor;
        if (cursor) { cursor.active = false; }
        node.active = true;
        this.setState({cursor: node});
    }

    private onFilterKeyUp(e) {
        const filter = e.target.value.trim();
        if (!filter) {
            this.setState({ filterKeyword: "" });
        } else {
            this.setState({ filterKeyword: filter });
        }
    }

    render() {
        var treeData;
        if (this.state.filterKeyword.length === 0) {
            treeData = this.props.treeData;
        } else {
            const filteredTree = filters.filterTree(this.props.treeData, this.state.filterKeyword);
            treeData = filters.expandFilteredNodes(filteredTree, this.state.filterKeyword);
        }
        return (
            <div>
                <div className="digraph-form tree-searchbox">
                    <label style={{ marginTop: "0px" }}>Search File Tree</label>
                    <input style={{ width: "calc(100% - 25px)" }}
                            onKeyUp={this.onFilterKeyUp.bind(this)}
                            type="text"/>
                </div>
                <div className="tree-container">
                    <Treebeard
                        style={treeStyle}
                        data={treeData}
                        decorators={decorators}
                        onToggle={this.onToggle}
                    />
                </div>
            </div>
        );
    }
}