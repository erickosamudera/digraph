import React from 'react'
import ReactDOM from "react-dom";
import ReactModal from "react-modal";
import { DigraphDOMContract } from "../../contract/DigraphDOMContract";

const createDialogStyle = function(width: number = 500, height: number= 500) {
    return {
        content : {
            position              : 'relative',
            top                   : '50%',
            left                  : '50%',
            right                 : 'auto',
            bottom                : 'auto',
            marginRight           : '-50%',
            width                 : width + 'px',
            height                : height + 'px',
            transform             : 'translate(-50%, -50%)',
            background            : '#212121'
        }
    };
}

interface DialogProps {
    isOpen: boolean,
    width?: number,
    height?: number,
    shouldCloseOnEsc?: boolean
}

export default class DigraphDialog extends React.Component<DialogProps> {
    constructor(props: DialogProps) {
        super(props);
    }

    static closeDialog() {
        ReactDOM.render(
            <DigraphDialog isOpen={false} />,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        );
    }

    render() {
        var shouldCloseOnEsc = true;
        if (this.props.shouldCloseOnEsc !== undefined) {
            shouldCloseOnEsc = this.props.shouldCloseOnEsc;
        }
        return <ReactModal
                    isOpen={this.props.isOpen}
                    shouldCloseOnEsc={shouldCloseOnEsc}
                    onRequestClose={DigraphDialog.closeDialog}
                    overlayClassName="digraph-dialog-overlay"
                    style={createDialogStyle(this.props.width, this.props.height)}>
                        {this.props.children}
                </ReactModal>;
    }
}