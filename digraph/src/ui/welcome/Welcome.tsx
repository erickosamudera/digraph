import * as React from "react";


export default class Welcome extends React.Component {
    render(): React.ReactNode {
        return <div id="welcome-container">
            <div id="welcome-page">
                <h1>Digraph Story Editor</h1>
            </div>
        </div>;
    }
}
