import React from 'react';
import Select from 'react-select';
import { DigraphVariableOperation } from '../../entity/DigraphVariable';
import { FormErrorList, SubmitResult } from '../../entity/Form';
import ActionValidateFormData from './validator/ActionValidateFormData';
import Convenience from '../../toolbox/Convenience';
import { ValidationRuleList } from '../../entity/Validation';
import DialogFormHeader from './component/DialogFormHeader';
import FormLabel from './component/FormLabel';

const operationOptions = [
    {value: DigraphVariableOperation.ASSIGN, label: DigraphVariableOperation.ASSIGN},
    {value: DigraphVariableOperation.ADD, label: DigraphVariableOperation.ADD}
]

const convertToSelectOptions = function(variableList : object | undefined) {
    var options = [];
    if (variableList) {
        for (var variableId in variableList) {
            options.push({value: variableId, label: variableId})
        }
    }
    return options;
}

interface SetVariableNodeFormProps {
    onFormSubmit: (formData: SetVariableFormData) => Promise<SubmitResult>,
    existingForm? : SetVariableFormData,
    variableList?: object
}

interface SetVariableNodeFormState {
    errors: FormErrorList,
    isNewVariable: boolean
    variableId: string
    description: string
    operation: DigraphVariableOperation,
    value: number
}

export interface SetVariableFormData {
    variableId: string,
    description: string,
    operation: DigraphVariableOperation,
    value: number
}

export default class SetVariableNodeForm extends 
            React.Component<SetVariableNodeFormProps, SetVariableNodeFormState>{

    private formState: SetVariableFormData

    constructor(props: SetVariableNodeFormProps) {
        super(props);
        this.state = {
            errors: {},
            isNewVariable: false,
            variableId: "",
            description: "",
            operation: DigraphVariableOperation.ASSIGN,
            value: 0
        }
        this.formState = {
            variableId: "",
            description: "",
            operation: DigraphVariableOperation.ASSIGN,
            value: 0
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleOperationChange = this.handleOperationChange.bind(this);
        this.handleVariableOptionChange = this.handleVariableOptionChange.bind(this);
        this.handleVariableValueChange = this.handleVariableValueChange.bind(this);
        this.handleVariableIdChange = this.handleVariableIdChange.bind(this);
        this.handleVariableDescriptionChange = this.handleVariableDescriptionChange.bind(this);
        this.handleNewVariableSwitch = this.handleNewVariableSwitch.bind(this);
    }
    
    private async handleSubmit(event: React.FormEvent) : Promise<void> {
        event.preventDefault();
        
        const setVariableValidationRules : ValidationRuleList = {
            "variableId": ["required"],
            "operation": ["required"],
            "value": ["required","between[0-100]"]
        }
        
        if (this.state.isNewVariable) { setVariableValidationRules["description"] = ["required"] }

        const validationErrors = new ActionValidateFormData(setVariableValidationRules, this.formState).execute();
        if (! Convenience.isObjectEmpty(validationErrors)) {
            this.setState({ errors: validationErrors });
            return;
        }

        this.props.onFormSubmit(this.formState)
            .then(submitResult => {
                if (! Convenience.isObjectEmpty(submitResult.errors)) {
                    this.setState({errors: submitResult.errors});
                    return;
                }
            })
        return;
    }

    private handleOperationChange(option: any) {
        this.formState.operation = option.value.trim();
        this.setState({operation: option.value});
    }
    
    private handleVariableIdChange(event: React.ChangeEvent<HTMLInputElement>) {
        const value = event.target.value;
        this.formState.variableId = value.trim();
        this.setState({variableId: value})
    }
    
    private handleVariableDescriptionChange(event: React.ChangeEvent<HTMLInputElement>) {
        const value = event.target.value;
        this.formState.description = value.trim();
        this.setState({description: value})
    }

    private handleVariableOptionChange(option: any) {
        this.formState.variableId = option.value.trim();
        this.setState({variableId: option.value})
    }

    private handleVariableValueChange(event: React.ChangeEvent<HTMLInputElement>) {
        const valueInt = parseInt(event.target.value);
        if (! isNaN(valueInt)) {
            this.formState.value = valueInt;
            this.setState({value: valueInt});
        }
    }
    
    private handleNewVariableSwitch(event: React.ChangeEvent<HTMLInputElement>) {
        const value = event.target.checked;
        this.setState({isNewVariable: value});
    }

    componentDidMount() {
        const existingForm = this.props.existingForm;
        if (existingForm) {
            this.setState({
                variableId: existingForm.variableId,
                description: existingForm.description,
                operation: existingForm.operation,
                value: existingForm.value
            });
            this.formState = {
                variableId: existingForm.variableId,
                description: existingForm.description,
                operation: existingForm.operation,
                value: existingForm.value
            };
        }
    }

    render() {
        const variableSelectOptions = convertToSelectOptions(this.props.variableList);

        return <form className="digraph-form" onSubmit={this.handleSubmit}>
            <DialogFormHeader title="Variable Form" />
            <div className="error">{ this.state.errors.form || "" }</div>
            
            <div className="row-flex-container">
                <div>
                    { ( this.state.isNewVariable ? 
                        <div>
                            <FormLabel vertical={true} title="Name" error={this.state.errors.variableId || ""} />
                            <input value={this.state.variableId || ""} type="text" onChange={this.handleVariableIdChange} />
                            <FormLabel vertical={true} title="Description" error={this.state.errors.description || ""} />
                            <input value={this.state.description || ""} type="text" onChange={this.handleVariableDescriptionChange} /> 
                        </div>
                    : 
                        <div>
                            <FormLabel vertical={true} title="Variable" error={this.state.errors.variableId || ""} />
                            <div style={{width: "200px"}}>
                                <Select value={{ label: this.state.variableId, value: this.state.variableId }} onChange={this.handleVariableOptionChange} options={variableSelectOptions} />
                            </div>
                        </div>
                    ) }
                    <input style={{ marginTop:"12px", marginBottom: "15px" }} onChange={this.handleNewVariableSwitch} type="checkbox" /> Introduce new variable
                </div> 
                <div style={{marginLeft: "15px"}}>
                    <FormLabel vertical={true} title="Operation" error={this.state.errors.operation || ""} />
                    <div className="short-w">
                        <Select value={{ label: this.state.operation, value: this.state.operation }} onChange={this.handleOperationChange} options={operationOptions} />
                    </div>
                </div>
                <div style={{marginLeft: "15px"}}>
                    <FormLabel vertical={true} title="Value" error={this.state.errors.value || ""} />
                    <input className="short-w" type="number" value={this.state.value || 0} onChange={this.handleVariableValueChange} />
                </div>
            </div>
        </form> 
    }
}