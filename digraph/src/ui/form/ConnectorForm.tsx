import React from 'react';
import { SubmitResult, FormErrorList } from '../../entity/Form';
import { ValidationRuleList } from '../../entity/Validation';
import ActionValidateFormData from './validator/ActionValidateFormData';
import Convenience from '../../toolbox/Convenience';
import DialogFormHeader from './component/DialogFormHeader';
import FormLabel from './component/FormLabel';

export interface ConnectorFormData {
    newScenarioName: string
}

interface ConnectorFormProps {
    onFormSubmit: (formData: ConnectorFormData) => Promise<SubmitResult>
}

interface ConnectorFormState {
    errors: FormErrorList
}

export default class ConnectorForm 
        extends React.Component<ConnectorFormProps, ConnectorFormState> {

    private formState: ConnectorFormData;

    constructor(props: ConnectorFormProps) {
        super(props);

        this.formState = {
            newScenarioName: "",
        }

        this.state = {
            errors: {}
        }

        this.handleScenarioNameChange = this.handleScenarioNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    private handleScenarioNameChange(event: React.ChangeEvent<HTMLInputElement>) {
        var eventValue = event.target.value;
        this.formState.newScenarioName = eventValue;
    }

    private async handleSubmit(event: React.FormEvent<HTMLFormElement>) : Promise<void> {
        event.preventDefault();

        const formRules : ValidationRuleList = {
            newScenarioName: ["required"]
        }
        const validateErrors = new ActionValidateFormData(formRules, this.formState).execute();
        
        if (! Convenience.isObjectEmpty(validateErrors)) {
            this.setState({ errors : validateErrors });
            return;
        }

        this.props.onFormSubmit(this.formState)
            .then(submitResult => {
                if (! Convenience.isObjectEmpty(submitResult.errors)) {
                    this.setState({ errors : submitResult.errors });
                }
            });

        return;
    }

    render() {
        return <form
        className="digraph-form"
        onSubmit={this.handleSubmit}>
            <DialogFormHeader title="Connector Form" />
            <div className="error">{this.state.errors.form || ""}</div>
            <FormLabel title="New Scenario File Name" error={this.state.errors.newScenarioName || ""} />
            <div style={{fontFamily:'monospace'}}>
                / <input onChange={this.handleScenarioNameChange} name="newScenarioName" type="text" /> .dgjson
            </div>
        </form>;
    }
}