import React from 'react';
import MonacoEditor from 'react-monaco-editor';
import {CancellationToken, editor, IDisposable, languages, Position} from 'monaco-editor';
import {FormErrorList, SubmitResult} from '../../entity/Form';
import {ValidationRuleList} from '../../entity/Validation';
import ActionValidateFormData from './validator/ActionValidateFormData';
import Convenience from '../../toolbox/Convenience';
import DialogFormHeader from './component/DialogFormHeader';
import FormLabel from './component/FormLabel';
import log from 'loglevel';

export interface StoryNodeFormData {
    title: string
    description: string
    content: string
    [key: string]: string
}

interface StoryNodeFormProps {
    handleConfigureMonaco: (monaco: any) => Promise<void>,
    handleSuggestions: (model: editor.ITextModel, position: Position, completionContext: languages.CompletionContext, token: CancellationToken) => Promise<languages.CompletionList>
    existingStory?: StoryNodeFormData,
    onFormSubmit: (formData: StoryNodeFormData) => Promise<SubmitResult>
}

interface StoryNodeFormState {
    title: string,
    description: string,
    content: string,
    isUpdate: boolean,
    errors: FormErrorList,
    [key: string]: string | boolean | FormErrorList
}

export default class StoryNodeForm extends React.Component<StoryNodeFormProps, StoryNodeFormState> {
    private storyForm: StoryNodeFormData;
    private completionItemDisposables: Array<IDisposable> = [];

    constructor(props: StoryNodeFormProps) {
        super(props);
        this.state = {
            title: "",
            description: "",
            content: "",
            isUpdate: false,
            errors: {}
        }
        this.storyForm = {
            title: "",
            description: "",
            content: "",
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleEditorChange = this.handleEditorChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.processExisting = this.processExisting.bind(this);
        this.editorWillMount = this.editorWillMount.bind(this);
        this.editorDidMount = this.editorDidMount.bind(this);
    }

    private editorDidMount(editor: any, monaco: any) {
    }

    private editorWillMount(monaco: any) {
        this.props.handleConfigureMonaco(monaco)
                .then(() => {
                    const suggestionDisposable = monaco.languages.registerCompletionItemProvider('digraph', {
                        triggerCharacters: [">", "[", "(", "{"],
                        provideCompletionItems: (
                            model: editor.ITextModel, 
                            position: Position,
                            completionContext: languages.CompletionContext,
                            token: CancellationToken
                        ) => {
                            return this.props.handleSuggestions(model, position, completionContext, token);
                        }
                    });
                    this.completionItemDisposables.push(suggestionDisposable);
                })
                .catch((err) => {
                    LOGGER.error("Failed to configure monaco", err);
                })

    }

    private processExisting(existingStory: StoryNodeFormData | undefined) {
        if (existingStory) {
            this.storyForm = existingStory;
            this.setState({title: existingStory.title});
            this.setState({description: existingStory.description});
            this.setState({content: existingStory.content});
            this.setState({isUpdate: true})
        }
    }

    private handleChange(event: React.ChangeEvent<HTMLInputElement>) {
        const eventName = event.target.name;
        const eventValue = event.target.value;
        this.setState({[eventName]: eventValue});
        this.storyForm[eventName] = eventValue.trim();
    }

    private handleEditorChange(newValue: string) {
        this.setState({content: newValue})
        this.storyForm.content = newValue.trim();
    }

    private async handleSubmit(event: React.FormEvent<HTMLFormElement>) : Promise<void> {
        event.preventDefault();
        
        const storyFormRule : ValidationRuleList = {
            title: ["required"],
            description: ["required"],
            content: ["required"]
        };

        const validationErrors = new ActionValidateFormData(storyFormRule, this.storyForm).execute();
        if (! Convenience.isObjectEmpty(validationErrors)) {
            this.setState({ errors: validationErrors });
            return;
        }
        
        this.props.onFormSubmit(this.storyForm)
            .then(submitResult => {
                if (! Convenience.isObjectEmpty(submitResult.errors)) {
                    this.setState({errors: submitResult.errors});
                }
            });
        return;
    }

    componentDidMount() {
        this.processExisting(this.props.existingStory)
    }

    componentWillUnmount() {
        this.completionItemDisposables.forEach((disposable) => { disposable.dispose(); });
        this.completionItemDisposables = [];
    }

    render() {
        return <form
        className="digraph-form"
        onSubmit={this.handleSubmit}>
            <DialogFormHeader title="Story Form" />
            <div className="error">{ this.state.errors.form || "" }</div>
            <FormLabel title="Title" error={this.state.errors.title || ""} />
            <input className="medium-w" value={this.state.title} onChange={this.handleChange} name="title" type="text" />
            <FormLabel title="Description" error={this.state.errors.description || ""} />
            <input className="long-w" value={this.state.description} onChange={this.handleChange} name="description" type="text" />
            <FormLabel title="Content" error={this.state.errors.content || ""} />
            <MonacoEditor 
                language="digraph" 
                width="850px" 
                height="420px"
                options={{
                    minimap: { enabled: false },
                    wordBasedSuggestions: false
                }}
                value={this.state.content}
                onChange={this.handleEditorChange}
                editorWillMount={this.editorWillMount}
                editorDidMount={this.editorDidMount}
                />
        </form>;
    }
}

const LOGGER = log.getLogger(StoryNodeForm.name);