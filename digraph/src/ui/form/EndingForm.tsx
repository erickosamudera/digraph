import React from 'react';
import { FormErrorList, SubmitResult } from '../../entity/Form';
import Convenience from '../../toolbox/Convenience';
import { ValidationRuleList } from '../../entity/Validation';
import ActionValidateFormData from './validator/ActionValidateFormData';
import DialogFormHeader from './component/DialogFormHeader';
import FormLabel from './component/FormLabel';

export interface EndingFormData {
    title: string,
    description: string
}

interface EndingFormProps {
    onFormSubmit: (formData: EndingFormData) => Promise<SubmitResult>
}

interface EndingFormState {
    errors: FormErrorList
    title: string,
    description: string
}

export default class EndingForm 
        extends React.Component<EndingFormProps, EndingFormState> {

    private formData: EndingFormData;

    constructor(props: EndingFormProps) {
        super(props);

        this.formData = {
            title : "",
            description: ""
        }

        this.state = {
            errors: {},
            title: "",
            description: ""
        }

        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    private handleTitleChange(event: React.ChangeEvent<HTMLInputElement>) {
        const eventValue = event.target.value;
        this.formData.title = eventValue.trim();
        this.setState({title: eventValue});
    }

    private handleDescriptionChange(event: React.ChangeEvent<HTMLInputElement>) {
        const eventValue = event.target.value;
        this.formData.description = eventValue.trim();
        this.setState({description: eventValue});
    }

    private async handleSubmit(event: React.FormEvent<HTMLFormElement>) : Promise<void> {
        event.preventDefault();
        const formData = this.formData;

        const formRule : ValidationRuleList = {
            title: ["required"],
            description: ["required"]
        };
        const validationErrors = new ActionValidateFormData(formRule, formData).execute();
        if (! Convenience.isObjectEmpty(validationErrors)) {
            this.setState({ errors : validationErrors });
            return;
        }
        
        this.props.onFormSubmit(formData)
            .then(submitResult => {
                if (! Convenience.isObjectEmpty(submitResult.errors)) {
                    this.setState({ errors : submitResult.errors });
                    return;
                }
            });
    }

    render() {
        return <form
        className="digraph-form"
        onSubmit={this.handleSubmit}>
            <DialogFormHeader title="Ending Form" />
            <div className="error">{this.state.errors.form}</div>
            <FormLabel title="Title" error={this.state.errors.title || ""} />
            <input value={this.state.title} onChange={this.handleTitleChange} type="text" />
            <FormLabel title="Description" error={this.state.errors.description || ""} />
            <input value={this.state.description} onChange={this.handleDescriptionChange} type="text" />
        </form>;
    }
}