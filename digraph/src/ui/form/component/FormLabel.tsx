import React from "react";

interface FormLabelProps {
    title: string,
    error?: Array<string>
    vertical?: boolean
}

export default class FormLabel extends React.Component<FormLabelProps> {
    render() {
        var className = "form-label";
        if (this.props.vertical) {
            className = "form-label column-flex-container"
        }
    return <div className={className}>
            <label>{ this.props.title }</label>
            <span className="error">{ this.props.error }</span>
        </div>
    }
}