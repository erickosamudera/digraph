import React from "react";
import DigraphDialog from "../../dialog/DigraphDialog";

interface FormHeaderProps {
    title: string
}

export default class DialogFormHeader extends React.Component<FormHeaderProps> {
    render() {
        return <div className="dialog-form-header">
                <h1 className="flex-auto">{ this.props.title }</h1>
                <button className="close-button" type="button" onClick={ DigraphDialog.closeDialog }>X</button>
                <button className="margin-l-s submit-button" type="submit">V</button>
            </div>
    }

}
