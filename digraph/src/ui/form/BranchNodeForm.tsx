import React from 'react';
import uuidv1 from 'uuid';
import { SubmitResult, FormErrorList } from '../../entity/Form';
import Convenience from '../../toolbox/Convenience';
import { ValidationRuleList } from '../../entity/Validation';
import ActionValidateFormData from './validator/ActionValidateFormData';
import DialogFormHeader from './component/DialogFormHeader';
import FormLabel from './component/FormLabel';

export interface Choice {
    condition: string,
    title: string,
    description: string
}

interface ChoiceList {
    [key: string]: Choice
}

export interface BranchNodeFormData {
    choices: ChoiceList,
    title: string,
    description: string
}

interface BranchNodeFormProps {
    onFormSubmit: (formData: BranchNodeFormData) => Promise<SubmitResult>,
    existingForm?: BranchNodeFormData,
    variableList: object
}

interface BranchNodeFormState {
    errors: any,
    title: string,
    description: string,
    choices: ChoiceList
}

export default class BranchNodeForm extends React.Component<BranchNodeFormProps, BranchNodeFormState>{
    private formState: BranchNodeFormData

    constructor(props: BranchNodeFormProps) {
        super(props);

        const startingChoices : ChoiceList = {
            [uuidv1()]: { condition: "", title: "", description: "" },
            [uuidv1()]: { condition: "", title: "", description: "" }
        }

        this.state = {
            errors: {},
            choices: startingChoices,
            description: "",
            title: ""
        }

        this.formState = {
            choices: startingChoices,
            title: "",
            description: ""
        }

        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddChoice = this.handleAddChoice.bind(this);
        this.handleDeleteChoice = this.handleDeleteChoice.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleChoiceTitleChange = this.handleChoiceTitleChange.bind(this);
        this.handleChoiceDescriptionChange = this.handleChoiceDescriptionChange.bind(this);
        this.handleChoiceConditionChange = this.handleChoiceConditionChange.bind(this);
        this.renderBranches = this.renderBranches.bind(this);
    }

    private handleTitleChange(event: React.ChangeEvent<HTMLInputElement>) {
        const eventValue = event.target.value;
        this.formState.title = eventValue;
        this.setState({title: eventValue})
    }
    
    private handleDescriptionChange(event: React.ChangeEvent<HTMLInputElement>) {
        const eventValue = event.target.value;
        this.formState.description = eventValue;
        this.setState({description: eventValue})
    }

    private async handleSubmit(event: React.FormEvent<HTMLFormElement>) : Promise<void> {
        event.preventDefault();

        const formErrors : any = {}
        const mainFormRules : ValidationRuleList = {
            title: ["required"],
            description: ["required"]
        }
        const mainFormErrors = new ActionValidateFormData(mainFormRules, this.formState).execute();
        Object.assign(formErrors, mainFormErrors);

        const allChoiceErrors : any = {};
        const choiceKeys = Object.keys(this.state.choices);
        choiceKeys.forEach((choiceId) => {
            const choice = this.state.choices[choiceId];
            const choiceRules : ValidationRuleList = {
                title: ["required"],
                description: ["required"]
            }
            const choiceErrors = new ActionValidateFormData(choiceRules, choice).execute();
            if (! Convenience.isObjectEmpty(choiceErrors)) {
                allChoiceErrors[choiceId] = choiceErrors;
            }
        });
        if (! Convenience.isObjectEmpty(allChoiceErrors)) {
            formErrors["choices"] = allChoiceErrors;
        }

        if (! Convenience.isObjectEmpty(formErrors)) {
            this.setState({ errors: formErrors })
            return;
        }
        
        this.props.onFormSubmit(this.formState).then((submitResult) => {
            if (! Convenience.isObjectEmpty(submitResult.errors)) {
                this.setState({ errors: submitResult.errors });
            }
        })

        return;
    }
    
    private handleAddChoice() {
        const newBranchId = uuidv1();
        const currentChoices = this.state.choices;

        this.formState.choices[newBranchId] = {condition: "", title: "", description: ""};
        currentChoices[newBranchId] = {condition: "", title: "", description: ""};
        this.setState({choices: currentChoices})
    }

    private handleDeleteChoice(choiceId: string) {
        const currentChoices = this.state.choices;
        if (choiceId in currentChoices) {
            delete currentChoices[choiceId];
            this.setState({choices: currentChoices})
            delete this.formState.choices[choiceId];
        }
    }

    private handleChoiceConditionChange(event: React.ChangeEvent<HTMLInputElement>, choiceId: string) {
        const eventValue = event.target.value;
        const choices = this.state.choices;
        choices[choiceId].condition = eventValue;
        this.setState({choices: choices});
        this.formState.choices[choiceId].condition = eventValue;
    }

    private handleChoiceTitleChange(event: React.ChangeEvent<HTMLInputElement>, choiceId: string) {
        const eventValue = event.target.value;
        const choices = this.state.choices;
        choices[choiceId].title = eventValue;
        this.setState({choices: choices});
        this.formState.choices[choiceId].title = eventValue;
    }

    private handleChoiceDescriptionChange(event: React.ChangeEvent<HTMLInputElement>, choiceId: string) {
        const eventValue = event.target.value;
        const choices = this.state.choices;
        choices[choiceId].description = eventValue;
        this.setState({choices: choices});
        this.formState.choices[choiceId].description = eventValue;
    }

    private renderBranches(choices: ChoiceList)  {
        const thisInstance = this;
        const choiceIds = Object.keys(choices);
        return <div style={{display: "flex", flexWrap: "wrap"}}>
            { choiceIds.map(function(choiceId: string, index: number) {
                var choiceErrors : FormErrorList = {};
                if (
                    thisInstance.state.errors.hasOwnProperty("choices") &&
                    thisInstance.state.errors.choices.hasOwnProperty(choiceId)
                ) {
                    choiceErrors = thisInstance.state.errors.choices[choiceId];
                }

                return <div className="branch-form" key={choiceId}>
                    <FormLabel vertical={true} title={ "Choice #" + (index + 1) + " Title"} error={choiceErrors.title || ""} />
                    <input 
                        type="text"
                        value={thisInstance.state.choices[choiceId].title} 
                        onChange={function(event) {thisInstance.handleChoiceTitleChange(event, choiceId)}} 
                    />
                    <FormLabel vertical={true} title="Description" error={choiceErrors.description || ""} />
                    <input 
                        type="text" 
                        value={thisInstance.state.choices[choiceId].description}
                        onChange={function(event) {thisInstance.handleChoiceDescriptionChange(event, choiceId)}} 
                    />
                    <FormLabel vertical={true} title="Condition" error={choiceErrors.condition || ""} />
                    <input 
                        type="text" 
                        value={thisInstance.state.choices[choiceId].condition}
                        onChange={function(event) {thisInstance.handleChoiceConditionChange(event, choiceId)}} 
                    />
                    <button type="button" className="close-button" onClick={function(event) {thisInstance.handleDeleteChoice(choiceId)}}>X</button>
                </div>;
            })}
        </div>
    }

    componentDidMount() {
        if (this.props.existingForm) {
            this.formState = this.props.existingForm;
            this.setState({
                title: this.props.existingForm.title,
                description: this.props.existingForm.description,
                choices: this.props.existingForm.choices
            })
        }
    }

    render() {
        return <form
        className="digraph-form"
        onSubmit={this.handleSubmit}>
            <DialogFormHeader title="Branch Form" />
            <div className="error">{ this.state.errors.form || "" }</div>
            <FormLabel title="Title" error={this.state.errors.title || ""} />
            <input value={this.state.title} onChange={this.handleTitleChange} name="title" type="text" />
            <FormLabel title="Description" error={this.state.errors.description || ""} />
            <input value={this.state.description} onChange={this.handleDescriptionChange} name="description" type="text" />
            <FormLabel title="Branches" />
            {this.renderBranches(this.state.choices)}
            <button type="button" onClick={this.handleAddChoice}>Add Branch</button>
        </form>;
    }
}