import React from 'react';
import DigraphDialog from '../dialog/DigraphDialog';
import { SubmitResult, FormErrorList } from '../../entity/Form';
import Convenience from '../../toolbox/Convenience';

interface ConfirmationFormProps {
    title: string
    onFormSubmit: () => Promise<SubmitResult>
}

interface ConfirmationFormState {
    errors: FormErrorList
}

export default class ConfirmationForm extends React.Component<ConfirmationFormProps, ConfirmationFormState> {
    constructor(props: ConfirmationFormProps) {
        super(props);

        this.state = {
            errors: {}
        }
        
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    private handleCancel(event: React.MouseEvent<HTMLButtonElement>) {
        DigraphDialog.closeDialog();
    }

    private async handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        this.props.onFormSubmit().then((submitResult) => {
            if (! Convenience.isObjectEmpty(submitResult.errors)) {
                this.setState({ errors: submitResult.errors });
            }
        })
    }

    render() {
        return <form
        className="digraph-form"
        onSubmit={this.handleSubmit}>
            <h1>{this.props.title}</h1>
            <h2>Are you sure?</h2>
            <div className="error">{ this.state.errors.form || "" }</div>
            <button type="button" onClick={this.handleCancel}>No</button>
            <button type="submit">Yes</button>
        </form>;
    }
}