import { ValidationRuleList } from "../../../entity/Validation";
import { FormErrorList } from "../../../entity/Form";
import Validators from "./Validator";

const getRuleIndex = (value: string, rules: Array<string>) : number => {
    return rules.indexOf(value);
}

export default class ActionValidateFormData {
    private ruleList: ValidationRuleList;
    private formData: {[key:string]: any};
    private errorList: FormErrorList;

    constructor(ruleList: ValidationRuleList, formData: {[key:string]: any}) {
        this.ruleList = JSON.parse(JSON.stringify(ruleList));
        this.formData = formData;
        this.errorList = {};
        this.registerToErrorList = this.registerToErrorList.bind(this);
        this.execute = this.execute.bind(this);
    }

    private registerToErrorList(fieldId: string, errors: Array<string>) {
        if (errors.length !== 0) {
            if (! this.errorList.hasOwnProperty(fieldId)) {
                this.errorList[fieldId] = [];
            }
            const currentErrors = this.errorList[fieldId];
            this.errorList[fieldId] = currentErrors.concat(errors);
        }
    }

    public execute() : FormErrorList {
        const formData = this.formData;
        const ruleList = this.ruleList;
        const keys = Object.keys(ruleList);
        for (var fieldId of keys) {
            const rules = ruleList[fieldId];
            const data = formData[fieldId];
            const requiredIndex = getRuleIndex("required", rules); 
            if (requiredIndex > -1) {
                const requiredErrors = Validators.validateRequired(fieldId, data);
                if (requiredErrors.length !== 0) {
                    this.registerToErrorList(fieldId, requiredErrors);
                    rules.splice(requiredIndex, 1);
                    continue;
                }
            } else if (data === null || data === undefined) {
                continue;
            }

            rules.forEach(rule => {
                var errors : Array<string> = [];
                if (rule.startsWith("is[")) {
                    errors = Validators.validateIs(rule, fieldId, data);
                }
                if (rule.startsWith("between[")) {
                    errors = Validators.validateBetween(rule, fieldId, data);
                }
                this.registerToErrorList(fieldId, errors);
            });
        }

        return this.errorList;
    }
}