import { sprintf } from 'sprintf-js';
import { ValidationMessage } from "../../../contract/StringLiteralContract";
import Try from '../../../meta/Try';

export default class Validators {
    public static validateRequired(fieldId: string, fieldData: any) : Array<string> {
        const error = [ sprintf(ValidationMessage.ERR_REQUIRED, fieldId) ];
        if (fieldData === undefined) {
            return error;
        } 
        if (fieldData === null) {
            return error;
        }
        if (typeof fieldData === "string" && fieldData.trim().length === 0) {
            return error;
        }
        return [];
    }

    public static getQualifier(rule: string) : Try<string> {
        const qualifierRegex = (/\[(.*?)\]/g);
        const matches = qualifierRegex.exec(rule);
        if (matches == null || matches.length != 2) {
            return Try.failure(new Error("invalid qualifier"));
        } 
        return Try.success(matches[1]);
    }

    public static validateIs(rule: string, fieldId: string, fieldData: any) : Array<string> {
        return this.getQualifier(rule)
            .map((qualifierType) => {
                if (typeof fieldData !== qualifierType) {
                    return [ sprintf(ValidationMessage.ERR_TYPE_MISMATCH, fieldId, type) ];
                }
                return [];
            })
            .recover((exception) => {
                return [ sprintf(ValidationMessage.ERR_INVALID_RULE, fieldId) ];
            })
            .get();
    }

    public static validateBetween(rule: string, fieldId: string, fieldData: any) : Array<string> {
        const validateNumberResult = this.validateIs("is[number]", fieldId, fieldData)
        if (validateNumberResult.length !== 0) { return validateNumberResult };

        return this.getQualifier(rule)
            .map((qualifierType) => {
                const bounds = qualifierType.split("-");
                if (bounds.length !== 2) { return [ sprintf(ValidationMessage.ERR_INVALID_RULE, fieldId) ] }
                const lowerBound = parseInt(bounds[0]);
                const upperBound = parseInt(bounds[1]);
        
                if (isNaN(lowerBound) || isNaN(upperBound)) { return [ sprintf(ValidationMessage.ERR_INVALID_RULE, fieldId) ] }
        
                if (fieldData > upperBound || fieldData < lowerBound) {
                    return [ sprintf(ValidationMessage.ERR_BETWEEN, fieldId, lowerBound, upperBound ) ];
                }
                
                return [];
            }).recover((exception) => {
                return [ sprintf(ValidationMessage.ERR_INVALID_RULE, fieldId) ];
            }).get();
    }
}