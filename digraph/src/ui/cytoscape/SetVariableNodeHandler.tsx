import { NodeSingular } from "cytoscape";
import ReactDOM from "react-dom";
import React from "react";
import DigraphDialog from "../dialog/DigraphDialog";
import { DigraphDOMContract } from "../../contract/DigraphDOMContract";
import { DigraphNodeContract } from '../../contract/DigraphNodeContract';
import CytoscapePosition from "../../entity/Position";
import { DigraphVariableOperation } from "../../entity/DigraphVariable";
import uuidv1 from 'uuid/v1'
import SetVariableNodeForm, { SetVariableFormData } from "../form/SetVariableNodeForm";
import * as log from 'loglevel';
import ConfirmationForm from "../form/ConfirmationForm";
import { SubmitResult } from "../../entity/Form";

export default class SetVariableNodeHandler {

    private cytocore: cytoscape.Core;
    private handleListAllVariables: Function;
    private handleSave: (cytocore: cytoscape.Core) => Promise<void>;
    private handleSetVariable: (variableId: string, setterId: string, description: string, operation: DigraphVariableOperation, value: number) => Promise<void>;
    private handleUnsetVariable: (variableId: string, setterId: string) => Promise<void>;

    constructor(
        cytoscapeInstance: cytoscape.Core,
        handleListAllVariables: Function,
        handleSetVariable: (variableId: string, setterId: string, description: string, operation: DigraphVariableOperation, value: number) => Promise<void>,
        handleUnsetVariable: (variableId: string, setterId: string) => Promise<void>,
        handleSave: (cytocore: cytoscape.Core) => Promise<void>
    ) {
        this.cytocore = cytoscapeInstance;
        this.handleListAllVariables = handleListAllVariables;
        this.handleSetVariable = handleSetVariable;
        this.handleUnsetVariable = handleUnsetVariable;
        this.handleSave = handleSave;
        this.handleAdd = this.handleAdd.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(
        targetNode: NodeSingular,
    ) {
        const handleUnsetVariable = this.handleUnsetVariable;
        const cytocore = this.cytocore;
        const handleSave = this.handleSave;
        const variableId = targetNode.data("variableId");
        const onFormSubmit = async function() : Promise<SubmitResult> {
            return handleUnsetVariable(variableId, targetNode.id())
                .then(() => {
                    cytocore.remove(targetNode);
                    return handleSave(cytocore);
                })
                .then(
                    () => {
                        DigraphDialog.closeDialog();
                        return { errors: {} };
                    },
                    (err: Error) => { return { errors: {"form": [ "Failed to edit variable, exception " + err ] }} }
                );
        }
        ReactDOM.render(
            <DigraphDialog isOpen={true}>
                <ConfirmationForm title="Deleting Set Variable Node" onFormSubmit={onFormSubmit} />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        )
    }

    handleEdit(
        targetNode: NodeSingular
    ) {
        const cytocore = this.cytocore;
        const handleSetVariable = this.handleSetVariable;
        const handleSave = this.handleSave;
        const onFormSubmit = async function(formState: SetVariableFormData) : Promise<SubmitResult> {
            return handleSetVariable(formState.variableId, targetNode.id(), formState.description, formState.operation, formState.value)
                .then(() => {
                    const variableId = formState.variableId;
                    const operation = formState.operation;
                    const description = formState.description;
                    const value = formState.value;
                    const title = variableId + " " + operation + " " + value
        
                    targetNode.data("title", title);
                    targetNode.data("variableId", variableId);
                    targetNode.data("description", description);
                    targetNode.data("operation", operation);
                    targetNode.data("value", value);
        
                    return handleSave(cytocore);
                })
                .then(
                    () => {
                        DigraphDialog.closeDialog();
                        return { errors: {} };
                    },
                    (err: Error) => { return { errors: {"form": [ "Failed to edit variable, exception " + err ] }} }
                );
        }
        const existingForm = {
            variableId: targetNode.data("variableId"),
            description: targetNode.data("description"),
            operation: targetNode.data("operation"),
            value: targetNode.data("value")
        }
        ReactDOM.render(
            <DigraphDialog width={800} height={400} isOpen={true}>
                <SetVariableNodeForm existingForm={existingForm} onFormSubmit={onFormSubmit}/>
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        )
    }

    handleAdd (
        renderPosition: CytoscapePosition, 
        sourceNode: NodeSingular, 
    ) {
        const handleSetVariable = this.handleSetVariable;
        const cytocore = this.cytocore;
        const newNodeId = uuidv1();
        const handleSave = this.handleSave;
        const onFormSubmit = async function(formState: SetVariableFormData) : Promise<SubmitResult> {
            return handleSetVariable(formState.variableId, newNodeId, formState.description, formState.operation, formState.value)
                    .then(() => {
                        const variableId = formState.variableId;
                        const operation = formState.operation;
                        const description = formState.description;
                        const value = formState.value;
                        const title = variableId + " " + operation + " " + value
            
                        cytocore.add({
                            "group": "nodes",
                            "data": {
                                "id": newNodeId, 
                                "title": title,
                                "digraph": 1,
                                "type": DigraphNodeContract.TYPE_SET_VARIABLE,
                                "variableId": variableId,
                                "description": description,
                                "operation": operation,
                                "value": value
                            },
                            "position": renderPosition 
                        });
            
                        cytocore.add({
                            "group": "edges",
                            "data": {"id": uuidv1(), "digraph": 1, "source": sourceNode.id(), "target": newNodeId},
                        });
                        
                        return handleSave(cytocore);
                    })
                    .then(
                        () => {
                            DigraphDialog.closeDialog();
                            return { errors: {} };
                        },
                        (err: Error) => { return { errors: {"form": [ "Failed to edit variable, exception " + err ] }} }
                    );
        }
        const variableList = this.handleListAllVariables();
        ReactDOM.render(
            <DigraphDialog width={800} height={400} isOpen={true}>
                <SetVariableNodeForm variableList={variableList} onFormSubmit={onFormSubmit}/>
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        )
    }

    
}