import { NodeSingular } from "cytoscape";
import ReactDOM from "react-dom";
import React from "react";
import DigraphDialog from "../dialog/DigraphDialog";
import CytoscapePosition from "../../entity/Position";
import uuidv1 from 'uuid/v1'
import BranchNodeForm, { BranchNodeFormData, Choice } from "../form/BranchNodeForm";
import { DigraphNodeContract } from "../../contract/DigraphNodeContract";
import { DigraphDOMContract } from "../../contract/DigraphDOMContract";
import ConfirmationForm from "../form/ConfirmationForm";
import log from 'loglevel';
import { SubmitResult } from "../../entity/Form";
import { FormSubmitMessage, BranchFormSubmitMessage } from "../../contract/StringLiteralContract";

export default class BranchNodeHandler {

    private cytocore: cytoscape.Core;
    private handleSave: (cytocore: cytoscape.Core) => Promise<void>;
    private handleListAllVariables: Function;

    constructor(
        cytocore: cytoscape.Core,
        handleSave: (cytocore: cytoscape.Core) => Promise<void>,
        handleListAllVariables: Function
    ) {
        this.cytocore = cytocore;
        this.handleSave = handleSave;
        this.handleListAllVariables = handleListAllVariables;
        this.handleAdd = this.handleAdd.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.addChoiceNode = this.addChoiceNode.bind(this);
    }

    addChoiceNode(parentId: string, choiceId: string, choice: Choice, renderPos: CytoscapePosition) {
        this.cytocore.add({
            "group": "nodes",
            "data": {
                "id": choiceId, 
                "title": choice.title, 
                "description": choice.description, 
                "condition": choice.condition,
                "parentId": parentId,
                "digraph": 1,
                "type": DigraphNodeContract.TYPE_CHOICE
            },
            "position": renderPos
        });
        this.cytocore.add({
            "group": "edges",
            "data": {"id": uuidv1(), "digraph": 1, "source": parentId, "target": choiceId},
        });
    }

    handleDelete(
        targetNode: NodeSingular
    ) {
        const cytocore = this.cytocore;
        const handleSave = this.handleSave;
        const onFormSubmit = async function() : Promise<SubmitResult> {
            const choices = targetNode.data("choices");
            for (var choiceId in choices) {
                LOGGER.info("Removing choice node, id:", choiceId);
                cytocore.remove("node[id=\"" + choiceId + "\"]");
            }
            LOGGER.info("Removing branch node, titled \"" + targetNode.data("title") + "\"");
            cytocore.remove(targetNode);
            
            return handleSave(cytocore)
                .then(() => {
                    DigraphDialog.closeDialog();
                    return { errors: {} }
                })
                .catch((err) => {
                    return { errors: { form: [ FormSubmitMessage.ERR_SAVE_FAILED ] } };
                });
        };
        ReactDOM.render(
            <DigraphDialog isOpen={true}>
                <ConfirmationForm title="Deleting Branch Node" onFormSubmit={onFormSubmit} />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        )
    }

    handleEdit(
        targetNode: NodeSingular
    ) {
        const cytocore = this.cytocore;
        const thisInstance = this;
        const existingChoices = JSON.parse(JSON.stringify(targetNode.data("choices")));
        const handleSave = this.handleSave;

        const onFormSubmit = async function(formState: BranchNodeFormData) : Promise<SubmitResult> {
            if (Object.keys(formState.choices).length < 2) {
                return { errors: { form: [ BranchFormSubmitMessage.ERR_MIN_TWO_CHOICES ] } }     
            }

            targetNode.data("title", formState.title);
            targetNode.data("description", formState.description);
            targetNode.data("choices", formState.choices)

            for (var choiceId in existingChoices) {
                const choiceData = formState.choices[choiceId];
                const choiceNode = cytocore.getElementById(choiceId);
                if (! formState.choices.hasOwnProperty(choiceId)) {
                    cytocore.remove(choiceNode)
                } else {
                    choiceNode.data("title", choiceData.title);
                    choiceNode.data("description", choiceData.description);
                    choiceNode.data("condition", choiceData.condition);
                }
            }

            var i = 0;
            for (var choiceId in formState.choices) {
                if (! existingChoices.hasOwnProperty(choiceId)) {
                    console.log("New choice! " + choiceId);
                    const choice = formState.choices[choiceId];
                    const renderPosition = targetNode.position();
                    const choiceNodePos = new CytoscapePosition(renderPosition.x + (i * 50), renderPosition.y + 250);
                    thisInstance.addChoiceNode(targetNode.id(), choiceId, choice, choiceNodePos)
                    i++;
                }
            }

            return handleSave(cytocore)
                .then(() => {
                    DigraphDialog.closeDialog();
                    return { errors: {} };
                })
                .catch((err) => {
                    return { errors: { form: [ FormSubmitMessage.ERR_SAVE_FAILED ] } }     
                });

        };

        const existingForm = {
            choices: targetNode.data("choices"),
            title: targetNode.data("title"),
            description: targetNode.data("description")
        };

        const variableList = this.handleListAllVariables();
        ReactDOM.render(
            <DigraphDialog isOpen={true}>
                <BranchNodeForm existingForm={existingForm} variableList={variableList} onFormSubmit={onFormSubmit} />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        );
    }

    handleAdd(
        renderPosition: CytoscapePosition, 
        sourceNode: NodeSingular,
    ) {
        const thisInstance = this;
        const cytocore = this.cytocore;
        const sourceNodeId = sourceNode.id();
        const handleSave = this.handleSave;

        const onFormSubmit = async function(formState: BranchNodeFormData) : Promise<SubmitResult> {
            if (Object.keys(formState.choices).length < 2) {
                return { errors: { form: [ BranchFormSubmitMessage.ERR_MIN_TWO_CHOICES ] } }     
            }

            const branchId = uuidv1();

            cytocore.add({
                "group": "nodes",
                "data": {
                    "id": branchId, 
                    "title": formState.title, 
                    "description": formState.description,
                    "digraph": 1,
                    "type": DigraphNodeContract.TYPE_BRANCH,
                    "choices": formState.choices,
                },
                "position": renderPosition
            });
            cytocore.add({
                "group": "edges",
                "data": {"id": uuidv1(), "digraph": 1, "source": sourceNodeId, "target": branchId},
            });
            
            var i = 0;
            for (var choiceId in formState.choices) {
                const choice = formState.choices[choiceId];
                const choiceNodePos = new CytoscapePosition(renderPosition.x + (i * 50), renderPosition.y + 200);
                thisInstance.addChoiceNode(branchId, choiceId, choice, choiceNodePos);
                i++;
            }

            return handleSave(cytocore)
                .then(() => {
                    DigraphDialog.closeDialog();
                    return { errors: {} }
                })
                .catch((err) => {
                    return { errors: { form: [ FormSubmitMessage.ERR_SAVE_FAILED ] } }     
                });
        };
        
        const variableList = this.handleListAllVariables();
        ReactDOM.render(
            <DigraphDialog width={800} height={800} isOpen={true}>
                <BranchNodeForm variableList={variableList} onFormSubmit={onFormSubmit} />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        );
    }

}

const LOGGER = log.getLogger(BranchNodeHandler.name);