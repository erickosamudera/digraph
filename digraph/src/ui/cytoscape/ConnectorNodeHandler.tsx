import ReactDOM from "react-dom";
import React from "react";
import DigraphDialog from "../dialog/DigraphDialog";
import { DigraphDOMContract } from "../../contract/DigraphDOMContract";
import CytoscapePosition from "../../entity/Position";
import ConnectorForm, { ConnectorFormData } from "../form/ConnectorForm";
import log from 'loglevel';
import { DigraphNodeContract } from "../../contract/DigraphNodeContract";
import uuidv1 from 'uuid/v1';
import ConfirmationForm from "../form/ConfirmationForm";
import { SubmitResult } from "../../entity/Form";
import { FormSubmitMessage, ConnectorSubmitMessage } from "../../contract/StringLiteralContract";

export default class ConnectorNodeHandler {

    private cytocore: cytoscape.Core;
    private handleSave: (cytocore: cytoscape.Core) => Promise<void>;
    private handleGotoScenario: (scenarioId: string) => Promise<void>;
    private handleDeleteScenario: (targetNodeId: string) => Promise<void>;
    private handleNewScenario: (startNodeId: string, newScenarioName: string) => Promise<string>;

    constructor(
        cytocore: cytoscape.Core,
        handleSave: (cytocore: cytoscape.Core) => Promise<void>,
        handleGotoScenario: (scenarioId: string) => Promise<void>,
        handleDeleteScenario: (targetNodeId: string) => Promise<void>,
        handleNewScenario: (startNodeId: string, newScenarioName: string) => Promise<string> 
    ) {
        this.cytocore = cytocore; 
        this.handleSave = handleSave;
        this.handleGotoScenario = handleGotoScenario;
        this.handleDeleteScenario = handleDeleteScenario;
        this.handleNewScenario = handleNewScenario;
        this.handleAdd = this.handleAdd.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    async handleGoto (
        targetNode: cytoscape.NodeSingular
    ) {
        const scenarioId = targetNode.id();
        const scenarioName = targetNode.data("title");
        LOGGER.info("Moving to scenario, id:", scenarioId, ", name:", scenarioName);

        this.handleGotoScenario(scenarioId)
                .catch((err) => LOGGER.error("Failed to go to other digraph file", err));
    }
    
    handleDelete (
        targetNode: cytoscape.NodeSingular
    ) {
        const cytocore = this.cytocore;
        const handleSave = this.handleSave;
        const handleDeleteScenario = this.handleDeleteScenario;
        const onFormSubmit = async function() : Promise<SubmitResult> {
            return handleDeleteScenario(targetNode.id())
                .then(() => {
                    cytocore.remove(targetNode);
                    return handleSave(cytocore);
                })
                .then(() => {
                    DigraphDialog.closeDialog();
                    return { errors: {} };
                })
                .catch((err) => {
                    LOGGER.error("Failed to delete connector", err);
                    return { errors: { form: [ ConnectorSubmitMessage.ERR_DELETE_FAILED ] }};
                })
        };
        ReactDOM.render(
            <DigraphDialog isOpen={true}>
                <ConfirmationForm title="Deleting Connector Node" onFormSubmit={onFormSubmit} />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        );
    }

    handleAdd (
        position: CytoscapePosition,
        sourceNodeId: string,
    ) {
        const cytocore = this.cytocore;
        const startNode = this.cytocore.nodes("node[type=\""+ DigraphNodeContract.TYPE_TERMINAL_START + "\"]")[0];

        const handleSave = this.handleSave;
        const handleNewScenario = this.handleNewScenario;
        const onFormSubmit = async function(formState: ConnectorFormData) : Promise<SubmitResult> {
            let scenarioName = formState.newScenarioName;

            if (! scenarioName.endsWith(".dgjson")) {
                scenarioName += ".dgjson";
            }

            return handleNewScenario(startNode.id(), scenarioName)
                .then((newNodeId) => {
                    LOGGER.info("Adding connector node with id", newNodeId);

                    const title = "Go to " + scenarioName;
                    cytocore.add({
                        "group": "nodes",
                        "data": {"id": newNodeId, "title": title, "digraph": 1, "type": DigraphNodeContract.TYPE_CONNECTOR},
                        "position": position
                    });
                    cytocore.add({
                        "group": "edges",
                        "data": {"id": uuidv1(), "digraph": 1, "source": sourceNodeId, "target": newNodeId},
                    });
                    return handleSave(cytocore);
                })
                .then(() => {
                    DigraphDialog.closeDialog();
                    return { errors: {} }
                })
                .catch((err) => {
                    LOGGER.error("Failed to add connector", err);
                    return { errors: { form: [ FormSubmitMessage.ERR_SAVE_FAILED ] } }
                });
        };
        ReactDOM.render(
            <DigraphDialog height={250} isOpen={true}>
                <ConnectorForm onFormSubmit={onFormSubmit} />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        );
    }
}

const LOGGER = log.getLogger(ConnectorNodeHandler.name);