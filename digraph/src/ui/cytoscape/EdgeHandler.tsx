import log from 'loglevel';

export default class EdgeHandler {

    private cytocore: cytoscape.Core;

    constructor(
        cytocore: cytoscape.Core,
    ) {
        this.cytocore = cytocore; 
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete (
        targetEdge: cytoscape.EdgeSingular
    ) {
        LOGGER.info("Removing edge, source:", targetEdge.data("source"), "target:", targetEdge.data("target"));
        this.cytocore.remove(targetEdge);
    }
}

const LOGGER = log.getLogger(EdgeHandler.name);