import {NodeSingular} from "cytoscape";
import StoryNodeForm, {StoryNodeFormData} from "../form/StoryNodeForm";
import ReactDOM from "react-dom";
import React from "react";
import DigraphDialog from "../dialog/DigraphDialog";
import CytoscapePosition from "../../entity/Position";
import uuidv1 from 'uuid/v1';
import {DigraphNodeContract} from "../../contract/DigraphNodeContract";
import {DigraphDOMContract} from "../../contract/DigraphDOMContract";
import ConfirmationForm from "../form/ConfirmationForm";
import log from 'loglevel';
import {SubmitResult} from "../../entity/Form";
import {FormSubmitMessage} from "../../contract/StringLiteralContract";
import {CancellationToken, editor, languages, Position} from "monaco-editor";

export class StoryNode {
    id: string;
    title: string;
    description: string;
    content: string;
    digraph: number = 1;
    type: string = DigraphNodeContract.TYPE_STORY;

    constructor(id: string, title: string, description: string, content: string) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.content = content;
    }
}

export default class StoryNodeHandler {

    private cytocore: cytoscape.Core;
    private handleSuggestions: (model: editor.ITextModel, position: Position, completionContext: languages.CompletionContext, token: CancellationToken) => Promise<languages.CompletionList>;
    private handleConfigureMonaco: (monaco: any) => Promise<void>;
    private handleSave: (cytocore: cytoscape.Core) => Promise<void>;

    constructor(
        cytocore: cytoscape.Core,
        handleSuggestions: (model: editor.ITextModel, position: Position, completionContext: languages.CompletionContext, token: CancellationToken) => Promise<languages.CompletionList>,
        handleConfigureMonaco: (monaco: any) => Promise<void>,
        handleSave: (cytocore: cytoscape.Core) => Promise<void>
    ) {
        this.cytocore = cytocore; 
        this.handleSave = handleSave;
        this.handleSuggestions = handleSuggestions;
        this.handleConfigureMonaco = handleConfigureMonaco;
        this.handleAdd = this.handleAdd.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(targetNode: NodeSingular) {
        const handleSave = this.handleSave;
        const cytocore = this.cytocore;
        const onFormSubmit = async function() : Promise<SubmitResult> {
            log.info("Removing story node, titled \"" + targetNode.data("title") + "\"");
            cytocore.remove(targetNode);

            return handleSave(cytocore)
                .then(() => { 
                    DigraphDialog.closeDialog();
                    return { errors: {} };
                })
                .catch((err) => {
                    LOGGER.error("Failed to save", err);
                    return { errors: { form: [ FormSubmitMessage.ERR_SAVE_FAILED ] }};
                });
        };
        ReactDOM.render(
            <DigraphDialog isOpen={true}>
                <ConfirmationForm title="Deleting Story Node" onFormSubmit={onFormSubmit} />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        )
    }

    handleEdit(targetNode: NodeSingular) {
        const handleSave = this.handleSave;
        const cytocore = this.cytocore;
        const onFormSubmit = async function(addNodeFormState: StoryNodeFormData) : Promise<SubmitResult> {
            targetNode.data("title", addNodeFormState.title);
            targetNode.data("description", addNodeFormState.description);
            targetNode.data("content", addNodeFormState.content);

            return handleSave(cytocore)
                .then(() => { 
                    DigraphDialog.closeDialog();
                    return { errors: {} };
                })
                .catch((err) => {
                    LOGGER.error("Failed to save", err);
                    return { errors: { form: [ FormSubmitMessage.ERR_SAVE_FAILED ] }};
                });
        };
        const title = targetNode.data("title");
        const description = targetNode.data("description");
        const content = targetNode.data("content");
        const storyNodeForm = {title: title, description: description, content: content}
        
        ReactDOM.render(
            <DigraphDialog width={900} height={750} shouldCloseOnEsc={false} isOpen={true}>
                <StoryNodeForm
                    handleSuggestions={this.handleSuggestions}
                    handleConfigureMonaco={this.handleConfigureMonaco}
                    existingStory={storyNodeForm}
                    onFormSubmit={onFormSubmit} 
                />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        );
    }
    
    handleAdd(position: CytoscapePosition, sourceNodeId: string) {
        const cytocore = this.cytocore;
        const handleSave = this.handleSave;
        const onFormSubmit = async function(addNodeFormState: StoryNodeFormData) : Promise<SubmitResult> {
            const newId = uuidv1();
            const connectionId = uuidv1();
            const newNode = new StoryNode(newId, addNodeFormState.title, addNodeFormState.description, addNodeFormState.content)
            
            cytocore.add({ "group": "nodes", "data": newNode, "position": position });
            cytocore.add({ "group": "edges", "data": {"id": connectionId, "digraph": 1, "source": sourceNodeId, "target": newId} });
            
            return handleSave(cytocore)
                .then(() => { 
                    DigraphDialog.closeDialog();
                    return { errors: {} };
                })
                .catch((err) => {
                    LOGGER.error("Failed to save", err);
                    return { errors: { form: [ FormSubmitMessage.ERR_SAVE_FAILED ] }};
                });
        };
        ReactDOM.render(
            <DigraphDialog width={900} height={750} shouldCloseOnEsc={false} isOpen={true}>
                <StoryNodeForm
                    handleSuggestions={this.handleSuggestions}
                    handleConfigureMonaco={this.handleConfigureMonaco}
                    onFormSubmit={onFormSubmit} 
                />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        );
    }
}

const LOGGER = log.getLogger(StoryNodeHandler.name);