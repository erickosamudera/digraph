import ReactDOM from "react-dom";
import React from "react";
import DigraphDialog from "../dialog/DigraphDialog";
import { DigraphDOMContract } from "../../contract/DigraphDOMContract";
import CytoscapePosition from "../../entity/Position";
import log from 'loglevel';
import { DigraphNodeContract } from "../../contract/DigraphNodeContract";
import uuidv1 from 'uuid/v1';
import ConfirmationForm from "../form/ConfirmationForm";
import EndingForm, { EndingFormData } from "../form/EndingForm";
import { SubmitResult } from "../../entity/Form";
import { FormSubmitMessage } from "../../contract/StringLiteralContract";

export default class EndingNodeHandler {

    private cytocore: cytoscape.Core;
    private handleSave: (cytocore: cytoscape.Core) => Promise<void>

    constructor(
        cytocore: cytoscape.Core,
        handleSave: (cytocore: cytoscape.Core) => Promise<void>
    ) {
        this.cytocore = cytocore; 
        this.handleSave = handleSave;
        this.handleAdd = this.handleAdd.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete (
        targetNode: cytoscape.NodeSingular
    ) {
        const cytocore = this.cytocore;
        const handleSave = this.handleSave;
        const onFormSubmit = async function() : Promise<SubmitResult> {
            LOGGER.info("Removing ending node, id:", targetNode.id())
            cytocore.remove(targetNode);
            return handleSave(cytocore)
                .then(() => {
                    DigraphDialog.closeDialog();
                    return { errors: {} };
                })
                .catch((err) => {
                    LOGGER.error("Failed to remove ending node", err);
                    return { errors: { form: [ FormSubmitMessage.ERR_SAVE_FAILED ] }};
                })
        };
        ReactDOM.render(
            <DigraphDialog isOpen={true}>
                <ConfirmationForm title="Deleting Connector Node" onFormSubmit={onFormSubmit} />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        );
    }

    handleAdd (
        position: CytoscapePosition,
        sourceNodeId: string,
    ) {
        const cytocore = this.cytocore;
        const handleSave = this.handleSave;
        const onFormSubmit = async function(formState: EndingFormData) : Promise<SubmitResult> {
            const newNodeId = uuidv1();
            const title = formState.title;
            const description = formState.description;
            cytocore.add({
                "group": "nodes",
                "data": {
                    "id": newNodeId, 
                    "title": title, 
                    "description": description,
                    "digraph": 1,
                    "type": DigraphNodeContract.TYPE_TERMINAL_END
                },
                "position": position
            });
            cytocore.add({
                "group": "edges",
                "data": {"id": uuidv1(), "digraph": 1, "source": sourceNodeId, "target": newNodeId},
            });
            return handleSave(cytocore)
                .then(() => {
                    DigraphDialog.closeDialog();
                    return { errors: {}};
                })
                .catch((err) => {
                    return { errors: { form: [ FormSubmitMessage.ERR_SAVE_FAILED ]}};
                });
        };
        ReactDOM.render(
            <DigraphDialog isOpen={true}>
                <EndingForm onFormSubmit={onFormSubmit} />
            </DigraphDialog>,
            document.querySelector(DigraphDOMContract.DIALOG_CONTAINER)
        );
    }
}

const LOGGER = log.getLogger(EndingNodeHandler.name);