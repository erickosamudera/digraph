const digraphNodeStyles = require("../../../../view/stylesheet/digraph-node-styles.json");

import React from 'react';
import ReactDOM from "react-dom";
import cytoscape, { NodeSingular } from "cytoscape";
import ContextMenu from '../contextmenu/ContextMenu';
import MenuItem from '../contextmenu/MenuItem';

import edgehandles from 'cytoscape-edgehandles';
import CytoscapePosition from '../../entity/Position';
import StoryNodeHandler from './StoryNodeHandler';
import SetVariableNodeHandler from './SetVariableNodeHandler';
import BranchNodeHandler from './BranchNodeHandler';
import ConnectorNodeHandler from './ConnectorNodeHandler';
import EndingNodeHandler from './EndingNodeHandler';
import { DigraphNodeContract } from '../../contract/DigraphNodeContract';

import log from 'loglevel';
import EdgeHandler from './EdgeHandler';

interface CytoscapeProps {
    setVariableNodeHandlerFactory: (cytocore: cytoscape.Core) => SetVariableNodeHandler,
    storyNodeHandlerFactory: (cytocore: cytoscape.Core) => StoryNodeHandler,
    branchNodeHandlerFactory: (cytocore: cytoscape.Core) => BranchNodeHandler,
    connectorNodeHandlerFactory: (cytocore: cytoscape.Core) => ConnectorNodeHandler,
    endingNodeHandlerFactory: (cytocore: cytoscape.Core) => EndingNodeHandler,
    handleSaveCytoscapeData: (cytoscapeJsonData: any) => Promise<void>
    jsonData: any
}

export default class DigraphCytoscape extends React.Component<CytoscapeProps> {
    private lastMouseUpPosX: number = 0;
    private lastMouseUpPosY: number = 0;
    private lastMouseUpRelativePosX: number = 0;
    private lastMouseUpRelativePosY: number = 0;

    constructor(props: CytoscapeProps) {
        super(props);
        this.handleMouseUp = this.handleMouseUp.bind(this);
        this.afterRender = this.afterRender.bind(this);
        this.createAfterActivityHandler = this.createAfterActivityHandler.bind(this);
    }
    
    private handleMouseUp(event: React.MouseEvent<HTMLDivElement>) {
        const boundingBox = event.target.getBoundingClientRect();
        this.lastMouseUpPosX = event.pageX;
        this.lastMouseUpPosY = event.pageY;
        this.lastMouseUpRelativePosX = event.pageX - boundingBox.x;
        this.lastMouseUpRelativePosY = event.pageY - boundingBox.y;
    }

    private createAfterActivityHandler(cytocore: cytoscape.Core) {
        const thisInstance = this;
        return function handleAfterActivity() {
            thisInstance.props.handleSaveCytoscapeData(cytocore.json())
                .catch((err) => {
                    LOGGER.error("Failed to save cytoscape data", err);
                })
        }
    }
    
    private afterRender() {
        const jsonData = this.props.jsonData;
        const cytoscapeInstance = cytoscape({
            container: document.getElementById('digraph-cytoscape'),
            elements: jsonData,
            style: digraphNodeStyles,
            layout: { name: 'preset' },
            zoomingEnabled: false
        });

        cytoscapeInstance.center();
        
        const handleAfterActivity = this.createAfterActivityHandler(cytoscapeInstance);
        
        const setVariableNodeHandler = this.props.setVariableNodeHandlerFactory(cytoscapeInstance);
        const storyNodeHandler = this.props.storyNodeHandlerFactory(cytoscapeInstance);
        const branchNodeHandler = this.props.branchNodeHandlerFactory(cytoscapeInstance);
        const connectorNodeHandler = this.props.connectorNodeHandlerFactory(cytoscapeInstance);
        const endingNodeHandler = this.props.endingNodeHandlerFactory(cytoscapeInstance);

        const edgeHandler = new EdgeHandler(cytoscapeInstance);
        
        const thisInstance = this;
        const props = this.props;
        
        cytoscapeInstance.on('cxttap', 'node[type="' + DigraphNodeContract.TYPE_TERMINAL_END + '"]', function(event) {
            const cxtMenuPos = { x: event.originalEvent.pageX, y: event.originalEvent.pageY }
            const targetNode = event.target;
            ReactDOM.render(
                <ContextMenu show={true} position={cxtMenuPos}>
                    <MenuItem title="Delete Node" onClick={function(){ storyNodeHandler.handleDelete(targetNode); }} /> 
                </ContextMenu>,
                document.querySelector("#digraph-context-menu")
            );
        });

        cytoscapeInstance.on('cxttap', 'node[type="story"]', function(event) {
            const cxtMenuPos = { x: event.originalEvent.pageX, y: event.originalEvent.pageY }
            const targetNode = event.target;
            ReactDOM.render(
                <ContextMenu show={true} position={cxtMenuPos}>
                    <MenuItem title="Edit Node" onClick={function() { storyNodeHandler.handleEdit(targetNode); }} />
                    <MenuItem title="Delete Node" onClick={function(){ storyNodeHandler.handleDelete(targetNode); }} /> 
                </ContextMenu>,
                document.querySelector("#digraph-context-menu")
            );
        });

        cytoscapeInstance.on('cxttap', 'node[type="set-variable"]', function(event) {
            const cxtMenuPos = { x: event.originalEvent.pageX, y: event.originalEvent.pageY }
            const targetNode = event.target;
            ReactDOM.render(
                <ContextMenu show={true} position={cxtMenuPos}>
                    <MenuItem title="Edit Node" onClick={function() { setVariableNodeHandler.handleEdit(targetNode) }} />
                    <MenuItem title="Delete Node" onClick={function() { setVariableNodeHandler.handleDelete(targetNode) }} />
                </ContextMenu>,
                document.querySelector("#digraph-context-menu")
            );
        });

        cytoscapeInstance.on('cxttap', 'node[type="branch"]', function(event) {
            const cxtMenuPos = { x: event.originalEvent.pageX, y: event.originalEvent.pageY }
            const targetNode = event.target;
            ReactDOM.render(
                <ContextMenu show={true} position={cxtMenuPos}>
                    <MenuItem title="Edit Node" onClick={function() { branchNodeHandler.handleEdit(targetNode) }} />
                    <MenuItem title="Delete Node" onClick={function() { branchNodeHandler.handleDelete(targetNode) }} />
                </ContextMenu>,
                document.querySelector("#digraph-context-menu")
            );
        });
        
        cytoscapeInstance.on('cxttap', 'edge', function(event) {
            const cxtMenuPos = { x: event.originalEvent.pageX, y: event.originalEvent.pageY }
            const targetEdge = event.target;
            ReactDOM.render(
                <ContextMenu show={true} position={cxtMenuPos}>
                    <MenuItem title="Delete Edge" onClick={function() { edgeHandler.handleDelete(targetEdge); }} />
                </ContextMenu>,
                document.querySelector("#digraph-context-menu")
            );
        });

        cytoscapeInstance.on('cxttap', 'node[type="' + DigraphNodeContract.TYPE_CONNECTOR +  '"]', function(event) {
            const cxtMenuPos = { x: event.originalEvent.pageX, y: event.originalEvent.pageY }
            const targetNode = event.target;
            ReactDOM.render(
                <ContextMenu show={true} position={cxtMenuPos}>
                    <MenuItem title="Go to Scenario" onClick={function() { connectorNodeHandler.handleGoto(targetNode) }} />
                    <MenuItem title="Delete Node" onClick={function() { connectorNodeHandler.handleDelete(targetNode) }} />
                </ContextMenu>,
                document.querySelector("#digraph-context-menu")
            );
        });

        cytoscapeInstance.edgehandles({
            edgeType: function(sourceNode: NodeSingular, _: NodeSingular) {
                const isConnected = sourceNode.data("isConnected");
                if (isConnected === true) { return null; }
                return "flat";
            },
            start: function(sourceNode: NodeSingular) {
                const connectedEdges = sourceNode.connectedEdges('edge[source="' + sourceNode.id() + '"]');
                const isConnected = connectedEdges.length > 0;
                sourceNode.data("isConnected", isConnected);
            },
            complete: function(sourceNode: any, targetNode: any, createdEles: any) {
                setTimeout(() => {
                    // a little bit of delay is needed so edgehandler's phantom node and edge 
                    // won't be saved as well
                    handleAfterActivity();
                }, 200);
            },
            cancel: function(sourceNode: NodeSingular, _: any) {
                const renderPosition = new CytoscapePosition(thisInstance.lastMouseUpPosX, thisInstance.lastMouseUpPosY);
                const panPosition = cytoscapeInstance.pan();
                const nodeX = thisInstance.lastMouseUpRelativePosX - panPosition.x;
                const nodeY = thisInstance.lastMouseUpRelativePosY - panPosition.y; 
                const nodePosition = new CytoscapePosition(nodeX, nodeY);

                LOGGER.info("Rendering context menu at window-wide position:", renderPosition);

                ReactDOM.render(
                    <ContextMenu show={true} position={renderPosition}>
                        <MenuItem title="New Story Node" onClick={function() { storyNodeHandler.handleAdd(nodePosition, sourceNode.id())}} /> 
                        <MenuItem title="New Set Variable Node" onClick={function() { setVariableNodeHandler.handleAdd(nodePosition, sourceNode) }} /> 
                        <MenuItem title="New Branch Node" onClick={function() { branchNodeHandler.handleAdd(nodePosition, sourceNode) }} /> 
                        <MenuItem title="New Scenario File Node" onClick={function() { connectorNodeHandler.handleAdd(nodePosition, sourceNode.id()) }} /> 
                        <MenuItem title="New Ending Node" onClick={function() { endingNodeHandler.handleAdd(nodePosition, sourceNode.id())}} /> 
                    </ContextMenu>,
                    document.querySelector("#digraph-context-menu")
                );
            }
        });
    }

    componentDidMount() {
        cytoscape.use(edgehandles);
        this.afterRender();
    }

    componentDidUpdate() {
        this.afterRender();
    }

    render() {
        return <div id="digraph-cytoscape-probe" onMouseUp={this.handleMouseUp} style={{height: "100vh"}}>
            <div id="digraph-cytoscape"></div>
        </div>
    }

}

const LOGGER = log.getLogger(DigraphCytoscape.name);