import React from 'react';
import ReactDOM from 'react-dom';

function getMenuStyle(isVisible : boolean, position) {
    const menuStyle = {
        position: "absolute",
        background: "#eee",
        color: "#212121",
        border: "1px solid #ccc",
        zIndex: 11,
        padding: "3px 0px 3px 0px",
        top: position.y,
        left: position.x,
        display: "none"
    };
    if (isVisible) {
        menuStyle.display = "block"
    }
    return menuStyle;
}

function getBgStyle(isVisible : boolean) {
    const bgStyle = {
        position: "absolute",
        top: "0px",
        left: "0px",
        background: "none",
        zIndex: 10,
        width: "100vw",
        height: "100vh",
        display: "none"
    }
    if (isVisible) {
        bgStyle.display = "block"
    }
    return bgStyle;
}

interface Position {
    x: number,
    y: number
}

interface ContextMenuProps {
    position: Position,
    show: boolean
}

export default class ContextMenu extends React.Component<ContextMenuProps> {
    constructor(props: ContextMenuProps) {
        super(props);
    }

    private closeMenu() {
        ReactDOM.render(
            <ContextMenu position={{x: 0, y: 0}} show={false} />,
            document.querySelector("#digraph-context-menu")
        )
    }

    render() {
        const position = this.props.position;
        const menuStyle = getMenuStyle(this.props.show, position);
        const bgStyle = getBgStyle(this.props.show);

        return <div>
            <div id="bg" onClick={this.closeMenu} style={bgStyle}></div>
            <div id="menu" onClick={this.closeMenu} style={menuStyle}>
                {this.props.children}
            </div>
        </div>
    }
}