import React from 'react';

interface MenuItemProps {
    title: string,
    onClick: Function
}

interface MenuItemState {
    mouseHover: boolean
}

export default class MenuItem extends React.Component<MenuItemProps, MenuItemState> {

    constructor(props) {
        super(props);
        this.state = { mouseHover: false }
        this.handleMouseEnter = this.handleMouseEnter.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
    }

    private handleMouseEnter() {
        this.setState({mouseHover: true})
    }

    private handleMouseLeave() {
        this.setState({mouseHover: false})
    }

    private menuStyle(mouseHover : boolean) {
        const itemStyle = {
            width: "200px",
            padding: "2px 2px 2px 25px",
            cursor: "pointer"
        }
        if (mouseHover) {
            itemStyle["background"] = "#BDBDBD";
        } else {
            itemStyle["background"] = "none";
        }
        return itemStyle;
    }

    render() {
        return <div 
            onMouseEnter={this.handleMouseEnter} 
            onMouseLeave={this.handleMouseLeave}
            onClick={this.props.onClick} style={this.menuStyle(this.state.mouseHover)}>
            {this.props.title}
        </div>
    }
}