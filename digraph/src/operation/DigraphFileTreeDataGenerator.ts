import dirTree from 'directory-tree';

export default class DigraphFileTreeDataGenerator {
    private _directory: string;
    
    constructor(directory: string) {
        this._directory = directory;
        this.generateFileTreeData = this.generateFileTreeData.bind(this);
    }

    generateFileTreeData() {
        const directory = this._directory;
        const rawTree = dirTree(directory, {extensions:/\.dg(.+)/});
        rawTree.toggled = true;
        return rawTree;
    }
}