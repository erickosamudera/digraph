import log from 'loglevel';
import { SceneList } from '../../entity/Scene';

const sceneFile = "scene.dgconf";

export default class SceneManager {
    private sceneList: SceneList = {};
    private handleLoadFile: (fileName: string) => Promise<any>
    private handleInitialise: (initFileSpec: {[key: string]: object}) => Promise<void>

    constructor(
        handleInitialise: (initFileSpec: {[key: string]: object}) => Promise<void>,
        handleLoadFile: (fileName: string) => Promise<any>
    ) {
        this.handleInitialise = handleInitialise;
        this.handleLoadFile = handleLoadFile;
        this.initialise = this.initialise.bind(this);
        this.listAllScenes = this.listAllScenes.bind(this);
    }

    listAllScenes() : SceneList {
        return this.sceneList;
    }

    async initialise() : Promise<void> {
        return this.handleInitialise({ [sceneFile] : {} })
            .then(() => {
                return this.handleLoadFile(sceneFile)
            })
            .then(sceneList => {
                this.sceneList = sceneList;
            })
    }

}

const LOGGER = log.getLogger(SceneManager.name);