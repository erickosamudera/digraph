import uuidv1 from 'uuid';
import { DigraphNodeContract } from '../../contract/DigraphNodeContract';
import log from 'loglevel';
import DirectoryHelper from '../file/DirectoryHelper';
import Try from '../../meta/Try';

const initStartFile = "start.dgjson";
const getInitialState = function(initScenarioId: string, title: string) {
    return [
        {
            "data": {
                "id": initScenarioId, 
                "title": title,
                "digraph": 1,
                "type": DigraphNodeContract.TYPE_TERMINAL_START
            }
        }
    ];
};

export default class ScenarioManager {

    private handleInitialise: (initSpec: {[fileName: string]: object}) => Promise<void>;
    private handleAddDirectory: (dirPath: string) => Promise<void>;
    private handleAddFile: (fileName: string, jsonData: object) => Promise<void>;
    private handleDeleteFile: (fileName: string) => Promise<void>;
    private handleAddFileToIndex: (startNodeId: string, newNodeId: string, fileName: string) => Promise<void>;
    private handleGetFileNameFromIndex: (nodeId: string) => Promise<string>; 
    private handleDeleteFileFromIndex: (nodeId: string) => Promise<void>; 

    constructor(
        handleInitialise: (initSpec: {[fileName: string]: object}) => Promise<void>,
        handleAddDirectory: (dirPath: string) => Promise<void>,
        handleAddFile: (fileName: string, jsonData: object) => Promise<void>,
        handleDeleteFile: (fileName: string) => Promise<void>,
        handleAddFileToIndex: (startNodeId: string, newNodeId: string, fileName: string) => Promise<void>,
        handleGetFileNameFromIndex: (nodeId: string) => Promise<string>,
        handleDeleteFileFromIndex: (nodeId: string) => Promise<void>,
    ) {
        this.handleInitialise = handleInitialise;
        this.handleAddDirectory = handleAddDirectory;
        this.handleAddFile = handleAddFile;
        this.handleDeleteFile = handleDeleteFile;
        this.handleAddFileToIndex = handleAddFileToIndex;
        this.handleGetFileNameFromIndex = handleGetFileNameFromIndex;
        this.handleDeleteFileFromIndex = handleDeleteFileFromIndex;
        this.addNewScenarioAsync = this.addNewScenarioAsync.bind(this);
        this.deleteScenarioAsync = this.deleteScenarioAsync.bind(this);
        this.initialise = this.initialise.bind(this);
    }

    async initialise(initScenarioId: string) : Promise<void> {
        LOGGER.info("Initiating scenario module.");
        const initStartData = getInitialState(initScenarioId, "Start");
        return this.handleInitialise({[initStartFile]: initStartData});
    }

    async deleteScenarioAsync(targetNodeId: string) : Promise<void> {
        return this.handleGetFileNameFromIndex(targetNodeId)
            .then(
                (fileName) => this.handleDeleteFile(fileName),
                (err) => Promise.resolve()
            )
            .then(
                () => this.handleDeleteFileFromIndex(targetNodeId)
            );
    }

    async addNewScenarioAsync(startNodeId: string, fileName: string) : Promise<string> {
        const newNodeId = uuidv1();
        const initStartData = getInitialState(newNodeId, "Start of " + fileName);
        const fileDirectory = DirectoryHelper.getDirectoryOf(fileName);
        
        return this.handleAddDirectory(fileDirectory)
            .then(() => this.handleAddFile(fileName, initStartData))
            .then(() => this.handleAddFileToIndex(startNodeId, newNodeId, fileName))
            .then(() => newNodeId);
    }
}

const LOGGER = log.getLogger(ScenarioManager.name);