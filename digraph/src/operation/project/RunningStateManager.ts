import { RunningState, RunningStateKey } from "../../entity/RunningState";

const runningStateFile = "state.dgconf";

export default class RunningStateManager {
    private handleInitialise: (initFileSpec: {[key: string]: object}) => Promise<void>;
    private handleSaveFile: (saveFileSpec: {[key: string]: object}) => Promise<void>;
    private handleLoadFile: (filePath: string) => Promise<RunningState>;

    constructor(
        handleInitialise: (initFileSpec: {[key: string]: object}) => Promise<void>,
        handleSaveFile: (saveFileSpec: {[key: string]: object}) => Promise<void>,
        handleLoadFile: (filePath: string) => Promise<RunningState>
    ) {
        this.handleInitialise = handleInitialise;
        this.handleSaveFile = handleSaveFile;
        this.handleLoadFile = handleLoadFile;
        this.updateLastReadFile = this.updateLastReadFile.bind(this);
    }

    async initialise() : Promise<void> {
        return this.handleInitialise({ [runningStateFile] : {} });
    }

    async updateLastReadFile(fileName: string) : Promise<void> {
        return this.handleLoadFile(runningStateFile)
            .then(runningStateData => {
                runningStateData[RunningStateKey.LAST_READ_FILE] = fileName;
                return this.handleSaveFile({ [runningStateFile] : runningStateData });
            });
    }

    async getLastReadFile() : Promise<string> {
        return this.handleLoadFile(runningStateFile)
            .then((runningStateData) => {
                if (! runningStateData.hasOwnProperty(RunningStateKey.LAST_READ_FILE)) {
                    return "start.dgjson";
                }
                return runningStateData[RunningStateKey.LAST_READ_FILE];
            })
            .catch(err => {
                return "start.dgjson";
            })
    }

}
