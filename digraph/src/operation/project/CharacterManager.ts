import log from 'loglevel';
import { CharacterList } from "../../entity/Character";

const characterFile = "codex/characters.dgconf";

export default class CharacterManager {
    private characterData: CharacterList = {};
    private handleLoadFile: (fileName: string) => Promise<any>

    constructor(
        handleLoadFile: (fileName: string) => Promise<any>
    ) {
        this.handleLoadFile = handleLoadFile;
        this.initialise = this.initialise.bind(this);
        this.listAllCharacters = this.listAllCharacters.bind(this);
    }

    listAllCharacters() : CharacterList {
        return this.characterData;
    }

    async initialise() : Promise<void> {
        return this.handleLoadFile(characterFile)
            .then((charData) => {
                this.characterData = charData;
            })
    }

}

const LOGGER = log.getLogger(CharacterManager.name);