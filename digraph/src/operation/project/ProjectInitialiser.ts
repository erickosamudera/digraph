import ScenarioManager from './ScenarioManager';
import FileIndexManager from './FileIndexManager';
import VariableManager from './VariableManager';
import uuidv1 from 'uuid/v1';
import log from 'loglevel';
import CharacterManager from './CharacterManager';
import CodexManager from './CodexManager';
import RunningStateManager from './RunningStateManager';
import SceneManager from './SceneManager';

export default class ProjectInitialiser {
    private readonly variableManager: VariableManager;
    private readonly fileIndexManager: FileIndexManager;
    private readonly scenarioManager: ScenarioManager;
    private readonly characterManager: CharacterManager;
    private readonly codexManager: CodexManager;
    private readonly runningStateManager: RunningStateManager;
    private readonly sceneManager: SceneManager;

    constructor(
        variableManager: VariableManager, 
        fileIndexManager: FileIndexManager,
        scenarioManager: ScenarioManager,
        characterManager: CharacterManager,
        codexManager: CodexManager,
        runningStateManager: RunningStateManager,
        sceneManager: SceneManager
    ) {
        this.variableManager = variableManager;
        this.scenarioManager = scenarioManager;
        this.fileIndexManager = fileIndexManager;
        this.characterManager = characterManager;
        this.codexManager = codexManager;
        this.runningStateManager = runningStateManager;
        this.sceneManager = sceneManager;
        this.execute = this.execute.bind(this);
    }

    async execute() : Promise<void> {
        const initScenarioId = uuidv1();

        const initialisationTasks = [
            this.variableManager.initialise(),
            this.fileIndexManager.initialise(initScenarioId),
            this.scenarioManager.initialise(initScenarioId),
            this.characterManager.initialise(),
            this.codexManager.initialise(),
            this.runningStateManager.initialise(),
            this.sceneManager.initialise()            
        ];

        LOGGER.info("Running initialisation");
        return Promise.all(initialisationTasks)
            .then(() => Promise.resolve());
    }
}

const LOGGER = log.getLogger(ProjectInitialiser.name);