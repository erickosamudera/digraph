import DirectoryWriter from "../file/DirectoryWriter";
import FileDeleter from "../file/FileDeleter";
import DigraphFileTreeDataGenerator from "../DigraphFileTreeDataGenerator";
import log from 'loglevel';
import FileAccessChecker from '../file/FileAccessChecker';
import Try from "../../meta/Try";
import { writeJsonAsync } from "../file/FileIO";

export default class FileManager {
    private readonly projectDirectory: string;

    constructor(projectDirectory: string) {
        this.projectDirectory = projectDirectory;
        this.checkIfFileExists = this.checkIfFileExists.bind(this);
        this.fetchCurrentFileTree = this.fetchCurrentFileTree.bind(this);
        this.deleteFile = this.deleteFile.bind(this);
        this.addNewDirectory = this.addNewDirectory.bind(this);
        this.addNewFile = this.addNewFile.bind(this);
        this.getAbsolutePath = this.getAbsolutePath.bind(this);
    }

    checkIfFileExists (filePath: string) : Promise<boolean> {
        return new FileAccessChecker(this.projectDirectory + filePath).checkAsync();
    }

    fetchCurrentFileTree() {
        return new DigraphFileTreeDataGenerator(this.projectDirectory).generateFileTreeData();
    }

    getAbsolutePath(projectRelativePath: string) : string {
        return this.projectDirectory + projectRelativePath;
    }
    
    getRelativePath(absolutePath: string) : Try<string> {
        if (! absolutePath.startsWith(this.projectDirectory)) { 
            return Try.failure(new Error("Path is not in the project."))
        }
        return Try.success(absolutePath.substr(this.projectDirectory.length));
    }
    
    async deleteFile(filePath: string) : Promise<void> {
        LOGGER.info("Deleting file:", filePath);
        const fileDeleter = new FileDeleter(this.projectDirectory + filePath);
        return fileDeleter.deleteAsync();
    }

    async addNewDirectory(dirPath: string) : Promise<void> {
        LOGGER.info("Adding new directory:", dirPath);
        const dirWriter = new DirectoryWriter(this.projectDirectory + dirPath);
        return dirWriter.createDirectoryAsync();
    }

    async addNewFile(filePath: string, jsonData = {}) : Promise<void> {
        LOGGER.info("Adding new file:", filePath);
        return writeJsonAsync(this.projectDirectory + filePath, jsonData);
    }
}

const LOGGER = log.getLogger(FileManager.name);