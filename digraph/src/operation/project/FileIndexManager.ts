import * as log from 'loglevel';

const fileIndexName= "fileIndex.dgconf"

export interface FileIndex {
    name: string,
    connectedTo: Array<string>
}

interface FileIndexList {
    [key: string] : FileIndex
}

export default class FileIndexManager {
    private handleInitialise: (fileSpec: {[key: string]: object}) => Promise<void>;
    private handleSaveFile: (jsonData: {[key: string]: object}) => Promise<void>;
    private handleLoadFile: (filePath: string) => Promise<any>;

    constructor (
        handleInitialise: (initFileSpec: {[key: string]: object}) => Promise<void>,
        handleSaveFile: (saveFileSpec: {[key: string]: object}) => Promise<void>,
        handleLoadFile: (filePath: string) => Promise<FileIndexList>
    ) {
        this.handleInitialise = handleInitialise;
        this.handleSaveFile = handleSaveFile;
        this.handleLoadFile = handleLoadFile;
        this.registerNewFileToIndex = this.registerNewFileToIndex.bind(this);
        this.deleteFromIndex = this.deleteFromIndex.bind(this);
        this.getFileName = this.getFileName.bind(this);
        this.initialise = this.initialise.bind(this);
        this.mapFileIndexToJson = this.mapFileIndexToJson.bind(this);
    }

    private mapFileIndexToJson(fileIndexList: FileIndexList, fileIndexId: string, fileIndex: FileIndex) : FileIndexList {
        fileIndexList[fileIndexId] = {name: fileIndex.name, connectedTo: fileIndex.connectedTo}
        return fileIndexList;
    }

    async initialise(initScenarioId: string) : Promise<void> {
        const initFileIndexData = this.mapFileIndexToJson(
            {}, initScenarioId, {name: "start.dgjson", connectedTo: []}
        );
        const initFileSpec = { [fileIndexName]: initFileIndexData }
        return this.handleInitialise(initFileSpec);
    }

    async getFileName(connectorId: string) : Promise<string> {
        return this.handleLoadFile(fileIndexName)
            .then(fileIndexList => {
                if (fileIndexList.hasOwnProperty(connectorId)) {
                    const connectorData = fileIndexList[connectorId];
                    return Promise.resolve(connectorData.name);
                } else {
                    return Promise.reject(new Error("Connector id not registered in file index!"));
                }
            })
    }

    async deleteFromIndex(targetNodeId: string) : Promise<void> {
        return this.handleLoadFile(fileIndexName)
            .then(fileIndexList => {
                var modifiedIndexList = fileIndexList;
                delete modifiedIndexList[targetNodeId];
                for (var fileId in modifiedIndexList) {
                    const fileData = modifiedIndexList[fileId];
                    const filteredConnection = fileData.connectedTo.filter(value => value !== targetNodeId);
                    fileData.connectedTo = filteredConnection;
                }
                return this.handleSaveFile({[fileIndexName]: modifiedIndexList});
            })
    }
    
    async registerNewFileToIndex(startNodeId: string, newNodeId: string, fileName: string) : Promise<void> {
        return this.handleLoadFile(fileIndexName)
            .then((fileIndexList) => {
                fileIndexList[startNodeId].connectedTo.push(newNodeId);
                const fileIndex = { name: fileName, connectedTo: [] };
                const updatedJson = this.mapFileIndexToJson(fileIndexList, newNodeId, fileIndex);
                return this.handleSaveFile({[fileIndexName]: updatedJson});
            });
    }
}

const LOGGER = log.getLogger(FileIndexManager.name);