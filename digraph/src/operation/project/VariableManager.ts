import * as log from 'loglevel';
import { DigraphVariableOperation } from '../../entity/DigraphVariable';

const variableFileName = "variable.dgconf";

export interface VariableSetter {
    operation: string,
    value: number
}

export interface VariableSetterList {
    [key: string] : VariableSetter
}

export interface Variable {
    description: string,
    setter: VariableSetterList
}

export interface VariableList {
    [key: string] : Variable
}

const checkIfObjectEmpty = function(checkedObject: object) : boolean {
    return Object.keys(checkedObject).length === 0 && checkedObject.constructor === Object;
}

export default class VariableManager {
    private handleInitialise: (fileSpec: {[key: string]: object}) => Promise<void>;
    private handleSaveFile: (saveSpec: {[key: string]: object}) => Promise<void>;
    private handleLoadFile: (fileName: string) => Promise<any>;

    private variableList : VariableList;

    constructor(
        handleInitialise: (fileSpec: {[key: string]: object}) => Promise<void>,
        handleSaveFile: (saveSpec: {[key: string]: object}) => Promise<void>,
        handleLoadFile: (fileName: string) => Promise<any>
    ) {
        this.variableList = {};
        this.handleInitialise = handleInitialise;
        this.handleLoadFile = handleLoadFile;
        this.handleSaveFile = handleSaveFile;

        this.initialise = this.initialise.bind(this);
        this.listAllVariables = this.listAllVariables.bind(this);
        this.checkIfExistingVariable = this.checkIfExistingVariable.bind(this);
        this.setVariable = this.setVariable.bind(this);
        this.unsetVariable = this.unsetVariable.bind(this);
    }
    
    static getInitState() {
        return {};
    }

    private checkIfExistingVariable(variableId: string) {
        return Object.prototype.hasOwnProperty.call(this.variableList, variableId)
    }

    async initialise() : Promise<void> {
        return this.handleInitialise({[variableFileName]: {}})
            .then(() => {
                return this.handleLoadFile(variableFileName);
            })
            .then((variableData) => {
                this.variableList = variableData;
            });
    }

    listAllVariables() : VariableList {
        return this.variableList;
    }

    async unsetVariable(variableId: string, setterNodeId: string) : Promise<void> {
        LOGGER.info("Going to unset variable: ", variableId, " with setter: ", setterNodeId);
        
        const thisInstance = this;
        if (! this.checkIfExistingVariable(variableId)) {
            return Promise.reject(new Error("Variable not found, failing unset operation."));
        }

        LOGGER.info("Unsetting variable: ", variableId, " with setter: ", setterNodeId);
        const variableListCopy = JSON.parse(JSON.stringify(this.variableList));
        delete variableListCopy[variableId].setter[setterNodeId];

        if (checkIfObjectEmpty(variableListCopy[variableId].setter)) {
            LOGGER.info("No more setter for variable:", variableId, ", deleting variable.");
            delete variableListCopy[variableId];
        }

        return this.handleSaveFile({[variableFileName] : variableListCopy}).then(function() {
            LOGGER.info("Variable file saved, updating in-memory variable JSON")
            thisInstance.variableList = variableListCopy;
        })
    }

    async setVariable(
        variableId: string,
        setterNodeId: string, 
        description: string, 
        operation: DigraphVariableOperation, 
        setterValue: number
    ) : Promise<void> {
        LOGGER.info("Setting variable:", variableId, "with description:", description, "with setter:", setterNodeId, "with value:", setterValue);
        const thisInstance = this;
        const variableListCopy = JSON.parse(JSON.stringify(this.variableList));
        if (! this.checkIfExistingVariable(variableId)) {
            variableListCopy[variableId] = {
                description: description,
                setter: {[setterNodeId]: {operation: operation, value: setterValue}}
            }
        } else {
            variableListCopy[variableId].setter[setterNodeId] = {operation: operation, value: setterValue};
        }

        return this.handleSaveFile({[variableFileName]: variableListCopy}).then(function(){
            thisInstance.variableList = variableListCopy;
        });
    }
}

const LOGGER = log.getLogger(VariableManager.name);