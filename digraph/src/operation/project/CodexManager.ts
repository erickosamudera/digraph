import log from 'loglevel';
import { CodexType, Codex } from "../../entity/Codex";

const codexDir = "codex/";

const codexFiles : {[index: string] : string} = {
    [CodexType.CHARACTER]: "characters.dgconf",
    [CodexType.TERM]: "terms.dgconf",
    [CodexType.ENTITY]: "entities.dgconf",
    [CodexType.ITEM]: "items.dgconf",
    [CodexType.LOCATION]: "locations.dgconf",
}

export default class CodexManager {
    private codexData: Codex;
    private handleInitialise: (initSpec: {[key: string]: object}) => Promise<void>;
    private handleLoadFile: (fileName: string) => Promise<any>

    constructor (
        handleInitialise: (initSpec: {[key: string]: object}) => Promise<void>,
        handleLoadFile: (fileName: string) => Promise<any>
    ) {
        this.handleInitialise = handleInitialise;
        this.handleLoadFile = handleLoadFile;
        
        this.codexData = {};
        this.readCodexFiles = this.readCodexFiles.bind(this);
        this.getCodexData = this.getCodexData.bind(this);
        this.initialise = this.initialise.bind(this);
    }

    private async readCodexFiles() : Promise<void> {
        LOGGER.info("Reading all files in codex dir: ", codexDir);

        const codexKeys = Object.keys(codexFiles);
        const loadPromises = codexKeys.map(async key => {
            const codexFilePath = codexDir + codexFiles[key];
            return this.handleLoadFile(codexFilePath).then(function(codexObj) {
                return {
                    codexKey: key,
                    codexData: codexObj 
                }
            })
        });

        return Promise.all(loadPromises).then((loadPromiseList) => {
            loadPromiseList.forEach(loadResult => {
                this.codexData[loadResult.codexKey] = loadResult.codexData;
            });
        });
    }

    getCodexData() : Codex {
        return this.codexData;
    }

    async initialise() : Promise<void> {
        LOGGER.info("Initiating codex files...");
        const codexKeys = Object.keys(codexFiles);
        const initSpec : {[key: string]: object} = {};
        codexKeys.forEach(async key => {
            const fileName = codexFiles[key];
            const filePath = codexDir + fileName;
            initSpec[filePath] = {};
        });

        return this.handleInitialise(initSpec)
            .then(() => { return this.readCodexFiles(); });
    }

}

const LOGGER = log.getLogger(CodexManager.name);