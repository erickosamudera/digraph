import {Project} from "../../entity/Project";

export default class ProjectManager {
    private readonly handleLoadFile: (filePath: string) => Promise<any>;

    constructor(
        handleLoadFile: (filePath: string) => Promise<any>
    ) {
        this.handleLoadFile = handleLoadFile;
    }

    getProjectData(path: string) : Promise<Project> {
        return this.handleLoadFile(path).then(data => {
            if (data.hasOwnProperty("name") &&
            data.hasOwnProperty("description")) {
                let gameplayLanguage = [];
                if (data.hasOwnProperty("gameplayLanguage") && Array.isArray(data.gameplayLanguage)) {
                    gameplayLanguage = data.gameplayLanguage;
                }
                return new Project(data["name"], data["description"], gameplayLanguage);
            }
            throw new Error("Invalid project config.");
        });
    }


}