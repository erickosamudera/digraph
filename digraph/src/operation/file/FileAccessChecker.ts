import fs from 'fs';
import log from 'loglevel';

export default class FileAccessChecker {
    private filePath : string;
    constructor(filePath: string) {
        this.filePath = filePath;
        this.checkAsync = this.checkAsync.bind(this);
    }

    checkAsync() : Promise<boolean> {
        const filePath = this.filePath;
        return new Promise(function(resolve) {
            fs.access(filePath, fs.constants.F_OK | fs.constants.W_OK, (err) => {
                if (err) {
                    if (err.code === 'ENOENT') {
                        LOGGER.error(filePath, "does not exist");
                    } else {
                        LOGGER.error(filePath, "is not writable");
                    }
                    resolve(false);
                } else {
                    LOGGER.info(filePath, "exists, and it is writable");
                    resolve(true);
                }
            }); 
        });
    }
}

const LOGGER = log.getLogger(FileAccessChecker.name);