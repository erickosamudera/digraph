import * as fs from "fs";

export const readJsonAsync = function (filePath: string) : Promise<any> {
    return new Promise(function(resolve, reject) {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) { reject(err) }
            else {
                try {
                    const jsonData : any = JSON.parse(data);
                    resolve(jsonData);
                } catch (err) {
                    reject(err);
                }
            }
        });
    });
};
export const writeJsonAsync = function(filePath: string, data: object) : Promise<void> {
    return new Promise(function(resolve, reject) {
        const jsonString = JSON.stringify(data);
        fs.writeFile(filePath, jsonString, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};
