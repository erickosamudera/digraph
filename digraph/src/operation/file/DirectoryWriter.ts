import fs from 'fs';
import log from 'loglevel';

export default class DirectoryWriter {
    private directoryPath: string;
    constructor(directoryPath: string) {
        this.directoryPath = directoryPath;
        this.createDirectoryAsync = this.createDirectoryAsync.bind(this);
    }

    createDirectoryAsync() : Promise<void> {
        const directoryPath = this.directoryPath;
        return new Promise(function(resolve, reject) {
            if (fs.existsSync(directoryPath)) {
                LOGGER.info("File already exists")
                resolve();
            } else {
                fs.mkdir(directoryPath, { recursive: true }, (err) => {
                    if (err) { 
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            }
        });
    }
}

const LOGGER = log.getLogger(DirectoryWriter.name);