import rimraf from 'rimraf';
import log from 'loglevel';

export default class FileDeleter {
    private _filePath: string

    constructor(filePath: string) {
        this._filePath = filePath;
        this.deleteAsync = this.deleteAsync.bind(this);
    }

    deleteAsync() : Promise<void> {
        const filePath = this._filePath;
        return new Promise(function(resolve, reject) {
            rimraf(filePath, (err) => {
                if (err) { 
                    reject(err)
                } else {
                    resolve();
                }
            });
        });
    }
}

const LOGGER = log.getLogger(FileDeleter.name);