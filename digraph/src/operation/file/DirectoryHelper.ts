export default class DirectoryHelper {
    static getDirectoryOf(fileName: string) : string {
        const relativeDir = fileName.match(/(.*)[/\\]/);
        if (relativeDir !== null && relativeDir.constructor === Array) {
            return relativeDir[1] + "/";
        }
        return "";
    }
}