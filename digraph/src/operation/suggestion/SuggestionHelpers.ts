export default class SuggestionHelpers {
    static handleCreateDocumentation(entryId: string, otherNames: Array<string>, description: string) : string {
        const documentation : Array<string> = [];
        documentation.push("# " + entryId + "\n");
        otherNames.forEach((name, index) => {
            if (index === 0) {
                documentation.push("Other names:\n");
            }
            documentation.push("* " + name + "\n");
        });
        documentation.push("\n");
        documentation.push(description);
        return documentation.join("");
    }
}