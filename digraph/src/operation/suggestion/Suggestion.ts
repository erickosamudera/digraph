import {CancellationToken, editor, languages, Position} from "monaco-editor";
import {SuggestionEntry, SuggestionInputState} from "../../entity/Suggestion";
import {determineState, parseTriggerInfo, sanitiseInput} from "./InputParsing";
import {StageCommand} from "../../contract/DigraphStageContract";
import VariableManager, {VariableList} from "../project/VariableManager";
import CodexManager from "../project/CodexManager";
import SuggestionHelpers from "./SuggestionHelpers";
import {attachTriggerDeleter} from "./PostProcess";
import {Codex} from "../../entity/Codex";
import CharacterManager from "../project/CharacterManager";
import {
    getCharacterSuggestion,
    getCharacterVariant,
    getCodexEntry,
    getDigraphVariable,
    getEnclosingTagSuggestion,
    getLanguageSuggestion,
    getSceneSuggestion,
    getSceneVariant,
    getShowAdjective,
    getSingleTagSuggestion,
    getStageCommand,
    getStagePosition
} from "./ProduceSuggestion";
import {CharacterList} from "../../entity/Character";
import {SceneList} from "../../entity/Scene";
import SceneManager from "../project/SceneManager";


const handleNormal = function(
    allCharacters: CharacterList
) : Array<SuggestionEntry> {
    return getStageCommand().concat(
        getCharacterSuggestion(
            allCharacters, 
            SuggestionHelpers.handleCreateDocumentation,
            (charTrigger => { return charTrigger + " \"$0\"" })
        )
    );
};

const handleSpeechTag = function(
    gameplayLanguage: Array<string>
) : Array<SuggestionEntry> {
    return getLanguageSuggestion(gameplayLanguage)
        .concat(getSingleTagSuggestion())
        .concat(getEnclosingTagSuggestion())
};

const handleInlineIf = (
    allVariable: VariableList
) : Array<SuggestionEntry> => {
    return getDigraphVariable(allVariable);
};

const handleInlineVariant = (
    allCharacters: CharacterList,
    sanitisedInput: { before: string, after: string },
    model: editor.ITextModel,
    position: Position
) : Array<SuggestionEntry> => {

    const match = sanitisedInput.before.match(/^\s*?([\w-]+).*?\($/);
    if (match != null) {
        const char = match[1];
        return getCharacterVariant(allCharacters, char);
    }

    var currentPos = position.lineNumber;
    var whenChar = "";

    while (currentPos - 1 >= 1 && whenChar == "") {
        const previousLine = model.getValueInRange({
            startLineNumber: currentPos - 1,
            startColumn: 1,
            endLineNumber: currentPos,
            endColumn: 1
        });

        const lineMatch = previousLine.match(/^\s*?([\w-]+)\s*?when\s*\:/);
        if (lineMatch != null) {
            whenChar = lineMatch[1];
        }

        currentPos = currentPos - 1;
    }
    
    return getCharacterVariant(allCharacters, whenChar);
};

const handleIf = (
    sanitisedInput : { before: string, after: string },
    allVariable: VariableList
) : Array<SuggestionEntry> => {
    const before = sanitisedInput.before.split(" ").reverse();

    if (before.length >= 2 && before[1] == "if") {
        return getDigraphVariable(allVariable);
    }

    return [];
};

const handleHide  = function(
    sanitisedInput : { before: string, after: string },
    allCharacters : CharacterList
) : Array<SuggestionEntry> {

    const before = sanitisedInput.before.split(" ").reverse();
    if (before.length >= 2) {
        if (before[1] === "hide") {
            return getCharacterSuggestion(allCharacters, SuggestionHelpers.handleCreateDocumentation);
        }
    }

    return [];
};

const handleScene = function(
    sanitisedInput : { before: string, after: string },
    allScenes : SceneList
) : Array<SuggestionEntry> {

    const before = sanitisedInput.before.split(" ").reverse();

    var suggestion : Array<SuggestionEntry> = [];

    if (before.length >= 2 && before[1] === "scene") {
        suggestion = suggestion.concat(getSceneSuggestion(allScenes));
    }

    if (before.length >= 3 && before[2] === "scene") {
        const sceneTrigger = before[1];
        suggestion = suggestion.concat(getSceneVariant(allScenes, sceneTrigger));
    }

    return suggestion;
};

const handleShow = function(
    sanitisedInput : { before: string, after: string },
    allCharacters : CharacterList
) : Array<SuggestionEntry> {

    const before = sanitisedInput.before.split(" ").reverse();

    if (before[1] === "at") {
        return getStagePosition();
    }

    if (before.length >= 2) {
        if (before[1] === "show") {
            return getCharacterSuggestion(allCharacters, SuggestionHelpers.handleCreateDocumentation);
        }
        if (before[1] === "at") {
            return getStagePosition();
        }
    }

    if (before.length >= 3 && before[2] === "show") {
        const charTrigger = before[1];
        return getCharacterVariant(allCharacters, charTrigger)
                .concat(getShowAdjective());
    }

    if (before.length >= 4 && before[3] === "show") {
        return getShowAdjective();
    }

    return [];
};

const handleSpeech = (
    codex: Codex
) : Array<SuggestionEntry> => {
    return getCodexEntry(codex, SuggestionHelpers.handleCreateDocumentation);
};

export const handleSuggestion = function (
    gameplayLanguage: Array<string>,
    characterManager: CharacterManager,
    codexManager: CodexManager,
    variableManager : VariableManager,
    sceneManager : SceneManager
) : (model: editor.ITextModel, position: Position, completionContext: languages.CompletionContext, token: CancellationToken) => Promise<languages.CompletionList> {

    const allScenes : SceneList = sceneManager.listAllScenes();
    const allCharacters = characterManager.listAllCharacters();
    const allVariable = variableManager.listAllVariables();
    const allCodexEntry = codexManager.getCodexData();

    return (
        model: editor.ITextModel, position: Position, completionContext: languages.CompletionContext, token: CancellationToken
    ) => {
        // var match = textUntilPosition.match(/"dependencies"\s*:\s*{\s*("[^"]*"\s*:\s*"[^"]*"\s*,\s*)*("[^"]*)?$/);
        const triggerInfo = parseTriggerInfo(model, position);
        const sanitisedInput = sanitiseInput(triggerInfo);
        const inputState = determineState(sanitisedInput);

        let suggestions : Array<SuggestionEntry> = [];

        if (inputState.state === SuggestionInputState.NORMAL) {
            suggestions = handleNormal(allCharacters);
        }

        if (completionContext.triggerCharacter === "{" && inputState.state === SuggestionInputState.SPEECH_TAG) {
            suggestions = handleSpeechTag(gameplayLanguage);
        }
        
        if (completionContext.triggerCharacter === "(" && inputState.state === SuggestionInputState.VARIANT) {
            suggestions = handleInlineVariant(allCharacters, sanitisedInput, model, position);
        }

        if (completionContext.triggerCharacter === "[" && inputState.state === SuggestionInputState.IF) {
            suggestions = handleInlineIf(allVariable);
        } else if (inputState.state === SuggestionInputState.IF) {
            suggestions = handleIf(sanitisedInput, allVariable);
        }

        if (completionContext.triggerCharacter === ">" && inputState.state === SuggestionInputState.SPEECH) {
            suggestions = handleSpeech(allCodexEntry);
        }

        if (inputState.state === SuggestionInputState.COMMAND) {
            if (inputState.command === StageCommand.SHOW) {
                suggestions = handleShow(sanitisedInput, allCharacters);
            }
            if (inputState.command === StageCommand.HIDE) {
                suggestions = handleHide(sanitisedInput, allCharacters);
            }
            if (inputState.command === StageCommand.SCENE) {
                suggestions = handleScene(sanitisedInput, allScenes);
            }
        }

        if (completionContext.triggerCharacter === ">") {
            suggestions = attachTriggerDeleter(suggestions, position);
        }

        return Promise.resolve({
            suggestions: suggestions,
            incomplete: false
        })
    }
};

