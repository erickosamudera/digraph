import {CharacterList} from "../../entity/Character";
import {SuggestionEntry} from "../../entity/Suggestion";
import {DigraphSuggestionContract} from "../../contract/DigraphSuggestionContract";
import {StageCommand, StagePositioning} from "../../contract/DigraphStageContract";
import {VariableList} from "../project/VariableManager";
import {Codex} from "../../entity/Codex";
import {Language, languageList} from "../../contract/LanguageList";
import {SceneList} from "../../entity/Scene";
import {EnclosingTag, SingleTag} from "../../entity/SpeechTag";


export const getCodexEntry = function(
    codex: Codex,
    handleCreateDocumentation: (entryId: string, otherNames: Array<string>, description: string) => string
) : Array<SuggestionEntry> {
    const newArray : Array<SuggestionEntry> = [];

    Object.keys(codex).map(type => {
       const typeEntry = codex[type];

       Object.keys(typeEntry).map(entryId => {
           const entry = typeEntry[entryId];

           let otherNames : Array<string>;
           if (entry.hasOwnProperty("names")) {
               otherNames = entry.names;
           } else {
               otherNames = [];
           }

           const documentation = handleCreateDocumentation(entryId, otherNames, entry.description);
           const nameList = [entryId].concat(otherNames);

           nameList.forEach((name) => {
               const newCharacter = {
                   label: name,
                   detail: type,
                   kind: DigraphSuggestionContract.Value,
                   documentation: { value: documentation },
                   insertText: "[" + name + "](codex#" + entryId + ")"
               };
               newArray.push(newCharacter);
           });
       });
    });
    return newArray;
};

export const getCharacterVariant = function(
    allCharacter : CharacterList,
    charTrigger : string
) : Array<SuggestionEntry> {
    if (charTrigger === "") { return [] }

    const characters = Object.values(allCharacter);
    const filteredChar = characters.filter((character) => character.trigger == charTrigger);
    if (filteredChar.length == 1) {
        if (filteredChar[0].variant) {
            return filteredChar[0].variant.map(variant => {
                return {
                    label: variant,
                    detail: "variant",
                    kind: DigraphSuggestionContract.Property,
                    documentation: { value: "" },
                    insertText: variant
                }
            });
        }
    }
    return [];
};

export const getCharacterSuggestion = function(
    characterList : CharacterList,
    handleCreateDocumentation: (entryId: string, otherNames: Array<string>, description: string) => string,
    insertTextCallback : (charTrigger: string) => string = (charTrigger) => charTrigger,
) : Array<SuggestionEntry> {
    return Object.keys(characterList)
        .map(characterId => {
            const character = characterList[characterId];

            var otherNames : Array<string> = [];
            if (character.names != undefined &&
                character.hasOwnProperty("names")) {
                otherNames = character.names;
            }

            const documentation = handleCreateDocumentation(characterId, otherNames, character.documentation);

            character.keywords.sort();

            return {
                label: character.trigger,
                detail: "person",
                kind: DigraphSuggestionContract.Class,
                documentation: { value: documentation },
                insertText: insertTextCallback(character.trigger),
                insertTextRules: 4
            };
        });
};

export const getSceneSuggestion = function(
    sceneList: SceneList
) : Array<SuggestionEntry> {
    return Object.keys(sceneList)
            .map(sceneId => {
                const scene = sceneList[sceneId];
                return {
                    label: sceneId,
                    detail: "scene",
                    kind: DigraphSuggestionContract.Class,
                    documentation: { value: '' },
                    insertText: sceneId,
                    insertTextRules: 4
                };
            });
};

export const getSceneVariant = function(
    sceneList: SceneList,
    sceneTrigger : string
) : Array<SuggestionEntry> {
    if (sceneTrigger === "") { return [] }

    const filteredId = Object.keys(sceneList).filter((sceneId) => sceneId == sceneTrigger);
    if (filteredId.length == 1) {
        const filteredScene = sceneList[filteredId[0]]
        if (filteredScene.variant) {
            return filteredScene.variant.map(variant => {
                return {
                    label: variant,
                    detail: "variant",
                    kind: DigraphSuggestionContract.Property,
                    documentation: { value: "" },
                    insertText: variant
                }
            });
        }
    }
    return [];
};

export const getLanguageSuggestion = function(
    gameplayLanguages: Array<string>
) : Array<SuggestionEntry> {
    return gameplayLanguages.map(langCode => {
        var languageData : Language;

        if (languageList.hasOwnProperty(langCode)) {
            languageData = languageList[langCode];
        } else {    
            languageData = {
                name: "invalid",
                nativeName: "invalid"
            } 
        }

        return {
            label: "Lang: " + languageData.name,
            detail: "language",
            kind: DigraphSuggestionContract.Keyword,
            documentation: { value: "" },
            insertText: langCode + "}$0{/" + langCode + "}",
            insertTextRules: 4
        };
    });
};

export const getSingleTagSuggestion = function() : Array<SuggestionEntry> {
    return Object.values(SingleTag).map(tag => {
        return {
            label: tag,
            detail: "tag",
            kind: DigraphSuggestionContract.Keyword,
            documentation: { value: "" },
            insertText: tag + "}",
            insertTextRules: 4
        };
    });
};

export const getEnclosingTagSuggestion = function() : Array<SuggestionEntry> {
    return Object.values(EnclosingTag).map(tag => {
        return {
            label: tag,
            detail: "tag",
            kind: DigraphSuggestionContract.Keyword,
            documentation: { value: "" },
            insertText: tag + "}$0{/" + tag + "}",
            insertTextRules: 4
        };
    });
};

export const getShowAdjective = function() : Array<SuggestionEntry> {
    return [
        {
            label: "at",
            detail: "adjective",
            kind: DigraphSuggestionContract.Keyword,
            documentation: { value: "Determine the position where a character image will be shown." },
            insertText: "at"
        }
    ];
};

export const getStagePosition = function() : Array<SuggestionEntry> {
    return Object.values(StagePositioning).map(position => {
        return {
            label: position,
            detail: "position",
            kind: DigraphSuggestionContract.Value,
            documentation: { value: "" },
            insertText: position
        };
    });
};

export const getStageCommand = function () : Array<SuggestionEntry> {
    return Object.values(StageCommand).map(command => {
        return {
            label: "Command: " + command,
            detail: "command",
            kind: DigraphSuggestionContract.Function,
            documentation: { value: "" },
            insertText: command
        };
    });
};

export const getDigraphVariable = function (
    allVariables : VariableList
) : Array<SuggestionEntry> {
    return Object.keys(allVariables)
        .map((variableId) => {
            return {
                label: variableId,
                kind: DigraphSuggestionContract.Variable,
                insertText: variableId + " ",
            };
        });
};