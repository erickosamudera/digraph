import {Position} from 'monaco-editor';
import {SuggestionEntry} from "../../entity/Suggestion";

export const attachTriggerDeleter = function(
    suggestionList: Array<SuggestionEntry>,
    position: Position
) : Array<SuggestionEntry> {
    const additionalTextEdits = [
        {
            forceMoveMarkers: false,
            range: {
                startColumn: position.column - 1,
                endColumn: position.column,
                startLineNumber: position.lineNumber,
                endLineNumber: position.lineNumber
            },
            text: ""
        }
    ];
    return suggestionList.map((entry) => {
        entry.additionalTextEdits = additionalTextEdits;
        return entry;
    });
};