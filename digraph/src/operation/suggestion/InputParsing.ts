import { editor, Position, IRange } from "monaco-editor";
import { TriggerInfo, SuggestionType, SuggestionInputState } from "../../entity/Suggestion";
import { StageCommand } from "../../contract/DigraphStageContract";
import { string } from "prop-types";

export const parseTriggerInfo = (model: editor.ITextModel, currentPosition: Position) : TriggerInfo => {
    const beforeRangeInLine : IRange = {
        startLineNumber: currentPosition.lineNumber,
        startColumn: 1,
        endLineNumber: currentPosition.lineNumber,
        endColumn: currentPosition.column
    };
    const afterRangeInLine : IRange = {
        startLineNumber: currentPosition.lineNumber,
        startColumn: currentPosition.column,
        endLineNumber: currentPosition.lineNumber + 1,
        endColumn: 1
    };
    return {
        beforeText: model.getValueInRange(beforeRangeInLine),
        afterText: model.getValueInRange(afterRangeInLine)
    }
};

export const sanitiseInput = function(triggerInfo: TriggerInfo) : { before: string, after: string} {
    return {
        before : triggerInfo.beforeText.replace(/\\\"/g, ""),
        after : triggerInfo.afterText.replace(/\\\"/g, "")
    }
};

export const determineState = (sanitisedInput: { before : string, after : string }) : { state: SuggestionInputState, command? : StageCommand } => {
    const beforeQuoteCount = sanitisedInput.before.match(/\"/g);
    const afterQuoteCount = sanitisedInput.after.match(/\"/g);

    const beforeWordList = sanitisedInput.before.split(" ").reverse();
    
    if (
        beforeQuoteCount !== null && afterQuoteCount !== null
        && beforeQuoteCount.length >= 1
        && beforeQuoteCount.length === afterQuoteCount.length
    ) {
        for (var key in beforeWordList) {
            const word = beforeWordList[key];
            if (word.startsWith("{")) {
                return { state: SuggestionInputState.SPEECH_TAG }
            } 
            break;
        }
        return { state: SuggestionInputState.SPEECH };
    }

    if (! sanitisedInput.before.includes("\"")) {
        //TODO: make command recognition dynamic
        const commandList = Object.keys(StageCommand);
        for (var key in beforeWordList) {
            const word = beforeWordList[key];
            if ( word.startsWith("[") ) {
                return { state: SuggestionInputState.IF };
            }
            if ( word.startsWith("(") ) {
                return { state: SuggestionInputState.VARIANT };
            }
            if ( word == "if") {
                return { state: SuggestionInputState.IF };
            }
            if ( word == "show") {
                return { state: SuggestionInputState.COMMAND, command: StageCommand.SHOW };
            }
            if ( word == "hide") {
                return { state: SuggestionInputState.COMMAND, command: StageCommand.HIDE };
            }
            if ( word == "scene") {
                return { state: SuggestionInputState.COMMAND, command: StageCommand.SCENE };
            }
        }
    }
    
    return { state: SuggestionInputState.NORMAL };
};
