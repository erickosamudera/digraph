export const handleStoryMonaco = function() {
    return async function(monaco: any) : Promise<void> {

        monaco.languages.register({ id: 'digraph' });

        monaco.languages.setLanguageConfiguration('digraph', {
            brackets: [
                ['{', '}'],
                ['[', ']'],
                ['(', ')']
            ],
            autoClosingPairs: [
                { open: '{', close: '}' },
                { open: '[', close: ']' },
                { open: '(', close: ')' },
            ],
            surroundingPairs: [
                { open: '{', close: '}' },
                { open: '[', close: ']' },
                { open: '(', close: ')' },
            ],
            onEnterRules: [
                {
                    beforeText: /^\s*(?:if|elif|else).*?:\s*$/,
                    action: { indentAction: monaco.languages.IndentAction.Indent }
                },
                {
                    beforeText: /^.*?(?:when).*?:\s*$/,
                    action: { indentAction: monaco.languages.IndentAction.Indent }
                }
            ]
        });

        monaco.languages.setMonarchTokensProvider('digraph', {
            escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,
            langtags: /(eo|jv|cmn|en_ui)/,
            closedtags: /(fast|slow|small|large)/,
            singletags: /(instant|auto)/,
            constructs: /(show|hide|scene)/,
            tokenizer: {
                root: [
                    // [/when|show|if|else|desu/, "construct"],
                    { include: "@construct" },
                    [/^\s*?(?!@constructs)[\w-]+/, "speaker"],
                    [/"([^"\\]|\\.)*$/, 'speech.invalid' ],  // non-teminated string
                    [/"/,  { token: 'speech', next: '@speech' } ],
                ],
                construct: [
                    [/(^\s*?show\s+?)(\w+\s+?)(\w+)/, ["construct", "speaker", "variant"]],
                    [/(^\s*?scene\s+?)(\w+\s+?)(\w+)/, ["construct", "speaker", "variant"]],
                    [/(^\s*?hide\s+?)(\w+)/, ["construct", "speaker"]],
                    [/(at\ )(\w+)/, ['construct', 'position']],

                    [/(!?\[)((?:[^\]\\\"]|@escapes)*)(\]\([^\)\"]+\))/, ['tooltip.link', 'speech', 'tooltip.link']],
                    [/^\s*if/, { token: "construct", next: "@ifline"}],
                    [/\[/, { token: "delimiter", next: '@inlineif' }],
                    [/\(/, { token: "delimiter", next: '@inlinevariant' }],
                    [/^.*?(?:else).*?:/, { token: "construct"}],
                    [/when\s*:\s*$/, { token: "construct"}],
                ],
                speech: [
                    [/@escapes/, 'speech.escape'],
                    [/\\./,      'speech.escape.invalid'],
                    [/"/,        { token: 'speech', next: '@pop' } ],

                    // formatting
                    
                    { include: '@linecontent' },
                    { include: '@speechtag' },
                ],
                inlineif: [
                    [/else/, "construct"],
                    [/[^\]]+/, "if.condition"],
                    [/\]/, { token: 'delimiter', next: '@pop' } ],
                ],
                inlinevariant: [
                    [/[^\)]+/, "variant.name"],
                    [/\)/, { token: 'delimiter', next: '@pop' } ],
                ],
                ifline: [
                    [/[^:]+/, "if.condition"],
                    [/:/, { token: 'construct', next: '@pop' } ],
                ],
                linecontent: [
                    [/\*\*([^\\*\"]|@escapes|\*(?!\*))+\*\*/, 'speech.strong'],
                    [/\*([^\\*\"]|@escapes)+\*/, 'speech.emphasis'],
                    // links
                    [/(!?\[)((?:[^\]\\\"]|@escapes)*)(\])(\()([^\)\"]+)(\))/, ['', 'tooltip.text', '', '', 'tooltip.link', '']],
                ],
                speechtag: [
                    [/(\{)@langtags(\})(.*?)(\{\/)@langtags(\})/, ['speech.delimiter', 'tag', 'speech.delimiter', '', 'speech.delimiter', 'tag', 'speech.delimiter']],
                    [/(\{)@closedtags(\})(.*?)(\{\/)@closedtags(\})/, ['speech.delimiter', 'tag', 'speech.delimiter', '', 'speech.delimiter', 'tag', 'speech.delimiter']],
                    [/(\{)@singletags(\})/, ['speech.delimiter', 'tag', 'speech.delimiter']],
                ]
            }
        });

        monaco.editor.defineTheme('digraph-theme', {
            base: "vs-dark",
            inherit: true,
            colors: {
            },
            rules: [
                { token: '', foreground: '757575', background: '1E1E1E' },
                { token: "construct", foreground: 'EF9A9A' },
                { token: "position", foreground: 'E0E0E0' },
                { token: "if.condition", foreground: 'EEEEEE' },
                { token: "speaker", foreground: 'CE93D8' },
                { token: "variant", foreground: 'A5D6A7' },
                { token: "delimiter", foreground: 'E0E0E0'},
                { token: "speech.delimiter", foreground: '90CAF9'},
                { token: "tag", foreground: '90CAF9'},
                { token: "speech.strong", fontStyle: 'bold italic' },
                { token: "speech.emphasis", fontStyle: 'italic' },
                { token: "tooltip.text", foreground: '9E9E9E' },
                { token: "tooltip.link", fontStyle: 'underline' },
            ]
        });

        monaco.editor.setTheme('digraph-theme');
        
        return Promise.resolve();
    }
};