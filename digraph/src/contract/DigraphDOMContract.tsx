export enum DigraphDOMContract {
    DIGRAPH_CONTAINER = '#digraph-container',
    DIALOG_CONTAINER = '#digraph-dialog',
    TREE_CONTAINER = '#digraph-file-tree'
}
