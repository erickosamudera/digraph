export enum ValidationMessage {
    ERR_REQUIRED = "\"%s\" can't be empty.",
    ERR_INVALID_RULE = "\"%s\" has invalid rule.",
    ERR_TYPE_MISMATCH = "\"%s\" must be of %s type.",
    ERR_BETWEEN = "\"%s\" must be between %s and %s",
}

export enum ConnectorSubmitMessage {
    ERR_DELETE_FAILED = "Failed to delete scenario file, please try again."
}

export enum FormSubmitMessage {
    ERR_SAVE_FAILED = "Failed to save to file, please try again."
}

export enum BranchFormSubmitMessage {
    ERR_MIN_TWO_CHOICES = "You need at least two choices to form a branch."
}