export const StagePositioning = {
    LEFT_1: "left_1",
    LEFT_2: "left_2",
    LEFT_3: "left_3",
    CENTER: "center",
    RIGHT_1: "right_1",
    RIGHT_2: "right_2",
    RIGHT_3: "right_3"
};

export enum StageCommand {
    SHOW = "show",
    HIDE = "hide",
    SCENE = "scene"
}