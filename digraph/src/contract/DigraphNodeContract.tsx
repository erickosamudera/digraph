export enum DigraphNodeContract {
    TYPE_STORY = "story",
    TYPE_BRANCH = "branch",
    TYPE_CHOICE = "choice",
    TYPE_SET_VARIABLE = "set-variable",
    TYPE_CONNECTOR = "connector",
    TYPE_TERMINAL_START = "terminal-start",
    TYPE_TERMINAL_END = "terminal-end"
}