export interface Language {
    name: string,
    nativeName: string
}

export const languageList : {
    [key: string] : Language
} = {
    'eo' : {
        name: "Esperanto",
        nativeName: "Esperanto"
    },
    'jv' : {
        name: "Javanese",
        nativeName: "Basa Jawa"
    },
    'cmn' : {
        name: "Mandarin Chinese",
        nativeName: "官话"
    },
    'en_ui' : {
        name: "English - Robot UI",
        nativeName: "English UI"
    }
}