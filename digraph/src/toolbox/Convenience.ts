export default class Convenience {
    public static isObjectEmpty(object: object) : boolean {
        return Object.entries(object).length === 0 && object.constructor === Object
    }
}