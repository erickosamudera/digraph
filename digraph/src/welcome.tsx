import {DigraphDOMContract} from "./contract/DigraphDOMContract";
import Welcome from "./ui/welcome/Welcome";
import React from 'react';
import ReactDOM from "react-dom";

export const drawWelcomePage = function () {
    ReactDOM.render(
        <Welcome/>,
        document.querySelector(DigraphDOMContract.DIGRAPH_CONTAINER)
    );
};