export enum SingleTag {
    AUTO = "auto",
    INSTANT = "instant"
}

export enum EnclosingTag {
    FAST = "fast",
    SLOW = "slow",
    LARGE = "large",
    SMALL = "small"
}
