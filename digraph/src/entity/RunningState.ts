export interface RunningState {
    [key: string] : string
}

export enum RunningStateKey {
    LAST_READ_FILE = "lastReadFile"
}