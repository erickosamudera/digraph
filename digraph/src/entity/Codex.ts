export interface CodexEntry {
    names: Array<string>,
    description: string
}

export enum CodexType {
    LOCATION = "location",
    ENTITY = "entity",
    ITEM = "item",
    CHARACTER = "character",
    TERM = "term"
}

export interface CodexEntryList {
    [key: string] : CodexEntry
}

export interface Codex {
    [key: string] : CodexEntryList
}