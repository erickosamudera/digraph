export interface Scene {
    variant?: Array<string>
}

export interface SceneList {
    [key: string] : Scene
}