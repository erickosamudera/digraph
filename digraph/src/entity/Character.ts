export interface Character {
    names? : Array<string>,
    variant?: Array<string>,
    keywords: Array<string>,
    trigger: string,
    documentation: string
}

export interface CharacterList {
    [key: string] : Character
}