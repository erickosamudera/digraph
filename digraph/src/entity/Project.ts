export interface InitSpec {
    [key: string]: object
}

export class Project {
    constructor(
        readonly name: string,
        readonly description: string,
        readonly gameplayLanguage: Array<string>
    ) {}
}

export class ProjectState {
    constructor(
        readonly lastOpenProject: string
    ) {}
}