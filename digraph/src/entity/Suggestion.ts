import { editor } from "monaco-editor";

export interface SuggestionEntry {
    label: string,
    detail?: string,
    kind: any,
    documentation?: { value: string },
    insertText: string,
    insertTextRules?: number,
    additionalTextEdits?: Array<editor.ISingleEditOperation>
}

export interface SuggestionMap {
    [key: string]: Array<SuggestionEntry>
}

export interface TriggerInfo {
    beforeText: string,
    afterText: string
}

export enum SuggestionInputState {
    IF = "if",
    NORMAL = "normal",
    VARIANT = "variant",
    SPEECH = "speech",
    SPEECH_TAG = "speech_tag",
    COMMAND = "command"
}

export enum SuggestionType {
    NOTHING = "nothing",
    BASE = "base",
    DIALOGUE = "dialogue",
    VARIABLE = "variable",
    COMMAND_SHOW = "command_show",
    COMMAND_HIDE = "command_hide"
}