export interface ValidationResult {
    errors: {[key: string] : string}
}



export interface ValidationRuleList {
    [key: string] : Array<string>
}