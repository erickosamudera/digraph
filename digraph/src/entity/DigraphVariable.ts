export enum DigraphVariableOperation {
    ASSIGN = "=",
    ADD = "+"
}

export enum CompareOperation {
    CON_EQUAL = "==",
    CON_GT = ">",
    CON_GTE = ">=",
    CON_LT = "<",
    CON_LTE = "<=",
    CON_AND = "AND",
    CON_OR = "OR",
    CON_LEFT_PARENTHESIS = "(",
    CON_RIGHT_PARENTHESIS = ")"
}

export enum NumericalValue {
    VAL_0 = "0",
    VAL_1 = "1",
    VAL_2 = "2",
    VAL_3 = "3",
    VAL_4 = "4",
    VAL_5 = "5",
    VAL_6 = "6",
    VAL_7 = "7",
    VAL_8 = "8",
    VAL_9 = "9",
}

export class Setter {
    nodeId: String
    operation: DigraphVariableOperation
    value: number

    constructor(nodeId: string, operation: DigraphVariableOperation, value: number) {
        this.nodeId = nodeId;
        this.operation = operation;
        this.value = value;
    }
}

class SetterList {
    [nodeId: string] : Setter
}

export default class DigraphVariable {
    id: string;
    name: string;
    setters: SetterList;

    constructor(id: string, name: string, setters: SetterList) {
        this.id = id;
        this.name = name;
        this.setters = setters;
    }
}