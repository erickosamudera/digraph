export interface SubmitResult {
    errors: FormErrorList
}

export interface FormErrorList {
    [key: string]: Array<string>
}