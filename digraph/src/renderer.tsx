// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

import React from 'react';
import ReactModal from "react-modal";
import log from 'loglevel';
import ProjectManager from "./operation/project/ProjectManager";
import {readJsonAsync, writeJsonAsync} from "./operation/file/FileIO";
import {startDigraph} from "./digraph";
import {drawWelcomePage} from "./welcome";
import {ProjectState} from "./entity/Project";

log.setDefaultLevel("info");

ReactModal.setAppElement('body');

const LOGGER = log.getLogger("Renderer");

export const openWelcomePage = function() {
    drawWelcomePage();
};

export const getLastReadProject = function(stateFilePath: string) : Promise<string> {
    return readJsonAsync(stateFilePath).then(function(state: ProjectState) {
        return state.lastOpenProject;
    });
};

export const openProject = function(projectFile: string) : Promise<void> {
    return new ProjectManager(readJsonAsync).getProjectData(projectFile)
        .then((projectData) => {
            const match = projectFile.match(/(.*)[\/\\]/);
            if (match == null || match.length === 0) {
                throw new Error("Can't get project directory for file: \"" + projectFile + "\"");
            }
            const projectDir = match[0];
            return startDigraph(projectData, projectDir);
        });
};

export const saveLastOpenProject = function (stateFilePath: string, projectFile: string) {
    const projectState = new ProjectState(projectFile);
    writeJsonAsync(stateFilePath, projectState)
        .catch(err => LOGGER.error("Failed to save app state", err));
};