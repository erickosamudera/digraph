import { describe } from 'mocha';
import { assert } from 'chai';
import Convenience from '../../src/toolbox/Convenience';

describe("Convenience", () => {
    it ("isObjectEmpty should return true if object is empty", () => {
        assert.isTrue(Convenience.isObjectEmpty({}));
    });
    it ("isObjectEmpty should return false if object is not empty", () => {
        assert.isFalse(Convenience.isObjectEmpty({"not": "empty"}));
    });
})