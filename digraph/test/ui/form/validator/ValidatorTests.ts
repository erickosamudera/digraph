import { describe } from 'mocha';
import { assert } from 'chai';
import Validators from '../../../../src/ui/form/validator/Validator';

describe("Validator", () => {
    it("validateRequired should return error on undefined", () => {
        const test : {[key: string]: any} = {};
        const errors = Validators.validateRequired("something", test["takada"]);
        assert.isNotEmpty(errors);
    });

    it("validateRequired should return error on null", () => {
        const test : {[key: string]: any} = {"takada": null};
        const errors = Validators.validateRequired("something", test["takada"]);
        assert.isNotEmpty(errors);
    });

    it("validateRequired should return error if blank string", () => {
        const test : {[key: string]: any} = {"string_kosong": " "};
        const errors = Validators.validateRequired("something", test["string_kosong"]);
        assert.isNotEmpty(errors);
    });

    it("validateRequired should return blank object if everything is alright", () => {
        const test : {[key: string]: any} = {"ada_value": "bang"};
        const errors = Validators.validateRequired("something", test["ada_value"]);
        assert.isEmpty(errors);
    });

    it("getQualifier should return validator qualifier inside square bracket", () => {
        const qualifierPack = Validators.getQualifier("is[number]");
        const qualifier = qualifierPack.get();
        assert.strictEqual(qualifier, "number");
    });

    it("getQualifier should return exception if qualifier is invalid", () => {
        const qualifierPack1 = Validators.getQualifier("is[number");
        assert.throws(() => { qualifierPack1.get(); });
    });

    it("validateIs should return empty object if valid type", () => {
        const errors = Validators.validateIs("is[number]", "someField", 12);
        assert.isEmpty(errors);
    });

    it("validateIs should return error object if invalid type", () => {
        const errors = Validators.validateIs("is[number]", "someField", "duck");
        assert.isNotEmpty(errors);
    });

    it("validateIs should return error object if invalid rule", () => {
        const errors = Validators.validateIs("is[number", "someField", "duck");
        assert.isNotEmpty(errors);
    });

    it ("validateBetween should return error object if not number", () => {
        const errors = Validators.validateBetween("between[0-99]", "someField", "rock");
        assert.isNotEmpty(errors);
    });

    it ("validateBetween should return error object if outside range", () => {
        const errors = Validators.validateBetween("between[0-99]", "someField", 102);
        assert.isNotEmpty(errors);
    });

    it ("validateBetween should return error object if invalid range", () => {
        const upperBoundErrors = Validators.validateBetween("between[rock-hardplace]", "someField", 102);
        assert.isNotEmpty(upperBoundErrors);
        const lowerBoundErrors = Validators.validateBetween("between[rock-hardplace]", "someField", -2);
        assert.isNotEmpty(lowerBoundErrors);
    });

    it ("validateBetween should return empty object if inside range", () => {
        const errors = Validators.validateBetween("between[0-99]", "someField", 55);
        assert.isEmpty(errors);
    });

});