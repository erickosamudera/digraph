import { describe } from 'mocha';
import { assert } from 'chai';
import ActionValidateFormData from '../../../../src/ui/form/validator/ActionValidateFormData';
import { ValidationMessage } from '../../../../src/contract/StringLiteralContract';
import { sprintf } from 'sprintf-js';

describe("ActionValidateFormData", () => {
    it("execute should do its job", () => {
        const rules = {
            "variableId": ["required"],
            "description": ["required"],
            "operation": ["required"],
            "value": ["required","between[0-100]"]
        }
        const dummyFormData = {
            "value": 150
        }
        const errorList = new ActionValidateFormData(rules, dummyFormData).execute();
        const expectedErrorList = {
            "variableId": [ sprintf(ValidationMessage.ERR_REQUIRED, "variableId") ],
            "description": [ sprintf(ValidationMessage.ERR_REQUIRED, "description") ],
            "operation": [ sprintf(ValidationMessage.ERR_REQUIRED, "operation") ],
            "value": [ sprintf(ValidationMessage.ERR_BETWEEN, "value", 0, 100) ],
        }
        assert.deepStrictEqual(errorList, expectedErrorList);
    });

    it("execute should return empty object if there's no error", () => {
        const rules = {
            "variableId": ["required"],
            "description": ["required"],
            "operation": ["required"],
            "value": ["required","between[0-100]"]
        }
        const dummyFormData = {
            "variableId": "some_variable",
            "description": "Variable about something",
            "operation": "+",
            "value": 23
        }
        const errorList = new ActionValidateFormData(rules, dummyFormData).execute();
        const expectedErrorList = {};
        assert.deepStrictEqual(errorList, expectedErrorList);
    });
});