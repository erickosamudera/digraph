import { describe } from 'mocha';
import assert from 'assert';
import FileManager from '../../../src/operation/project/FileManager';
import fs from 'fs';
import rimraf from 'rimraf';
import uuidv4 from 'uuid/v4';

const dummyProjectDir = "/tmp/";

describe("File Manager", () => {
    it("getAbsolutePath should return absolute path from project relative path", () => {
        const fileManager = new FileManager(dummyProjectDir);
        const absolutePath = fileManager.getAbsolutePath("munu/desu/kwek.dgjson");
        assert.strictEqual(dummyProjectDir + "munu/desu/kwek.dgjson", absolutePath);
    });

    it("getRelativePath should return relative path of an absolutePath", () => {
        const fileManager = new FileManager(dummyProjectDir);
        const relativePath = fileManager.getRelativePath("/tmp/munu/desu/kwek.dgjson");
        assert.strictEqual("munu/desu/kwek.dgjson", relativePath.getOrElseGet(() => ""));
    });

    it("getRelativePath should return exception if path is not in the project", () => {
        const fileManager = new FileManager(dummyProjectDir);
        const relativePath = fileManager.getRelativePath("/tmpeee/munu/desu/kwek.dgjson");
        assert.strictEqual("wrong", relativePath.getOrElseGet(() => "wrong"));
    });

    it("addNewDirectory should create new directory relative to project dir", async () => {
        const fileManager = new FileManager(dummyProjectDir);

        await fileManager.addNewDirectory("a/b/c");
        
        const exists = fs.existsSync("/tmp/a/b/c/");
        assert.strictEqual(exists, true);
        rimraf(dummyProjectDir + "a/", {}, err => {});
    });

    it("addNewFile should create new file relative to project dir", async () => {
        const fileManager = new FileManager(dummyProjectDir);
        const fileId = uuidv4();
        
        await fileManager.addNewFile(fileId, {data: 123})

        const writtenData = fs.readFileSync(dummyProjectDir + fileId).toString();
        const parsedJson = JSON.parse(writtenData);
        assert.strictEqual(parsedJson.data, 123);

        rimraf(dummyProjectDir + fileId, {}, err => {});
    });

    it("deleteFile should delete file relative to project dir", async () => {
        const fileManager = new FileManager(dummyProjectDir);
        const fileId = uuidv4();

        fs.writeFileSync(dummyProjectDir + fileId, {});
        const exists = fs.existsSync(dummyProjectDir + fileId);
        assert.strictEqual(exists, true);

        await fileManager.deleteFile(fileId);

        const afterDeleteExists = fs.existsSync(dummyProjectDir + fileId);
        assert.strictEqual(afterDeleteExists, false);
    });
});