import { describe } from 'mocha';
import assert from 'assert';
import VariableManager from '../../../src/operation/project/VariableManager';
import { DigraphVariableOperation } from '../../../src/entity/DigraphVariable';

const dummyVariableList = {
    "variable_key_1": {
        "description": "Variable Description",
        "setter": {
            "nodeId1": {"operation": "=", "value": 1},
            "nodeId2": {"operation": "+", "value": 2},
        }
    },
    "variable_key_2": {
        "description": "Another Variable Description",
        "setter": {
            "nodeId3": {"operation": "-", "value": 3},
            "nodeId4": {"operation": "=", "value": 4},
        }
    }
};

describe("VariableManager", () => {
    it("initialise should initialise variable file", async () => {
        var actualInitSpec;
        const dummyInitialiser = async (initSpec: {[key: string]: object}) => {
            actualInitSpec = initSpec;
        }
        const variableManager = new VariableManager(
            dummyInitialiser, 
            async() => Promise.resolve(), 
            async() => {}
        );
        
        await variableManager.initialise();
        assert.deepStrictEqual(actualInitSpec, {"variable.dgconf": {}});
    });

    it("listAllVariables should return the current VariableList", async () => {
        const dummyLoadFile = async (fileName: string) => dummyVariableList
        const variableManager = new VariableManager(
            async() => Promise.resolve(), 
            async() => Promise.resolve(), 
            dummyLoadFile
        );
        
        await variableManager.initialise();
        
        assert.deepStrictEqual(variableManager.listAllVariables(), dummyVariableList);
    });
    
    it("unsetVariable should return false if variable is not found", async () => {
        const dummyLoadFile = async (fileName: string) => dummyVariableList
        const variableManager = new VariableManager(
            async() => Promise.resolve(), 
            async() => Promise.resolve(), 
            dummyLoadFile
        );
        await variableManager.initialise();
        assert.rejects( variableManager.unsetVariable("variable_key_3", "nodeId1") );
    });
    
    it("unsetVariable should return true and remove the setterId from variableList", async () => {
        var savedData = {};
        const dummyLoadFile = async (fileName: string) => dummyVariableList
        const dummySaveFile = async (saveSpec: {[fileName: string] : object}) => { savedData = saveSpec["variable.dgconf"]; }
        const variableManager = new VariableManager(
            async() => Promise.resolve(), 
            dummySaveFile, 
            dummyLoadFile
        );
        await variableManager.initialise();
        await variableManager.unsetVariable("variable_key_1", "nodeId2");

        const expectedVariableList = {
            "variable_key_1": {
                "description": "Variable Description",
                "setter": {
                    "nodeId1": {"operation": "=", "value": 1}
                }
            },
            "variable_key_2": {
                "description": "Another Variable Description",
                "setter": {
                    "nodeId3": {"operation": "-", "value": 3},
                    "nodeId4": {"operation": "=", "value": 4},
                }
            }
        }
        assert.deepStrictEqual(savedData, expectedVariableList);
    });

    it("unsetVariable should remove the variable entry altogether if there no more setter", async() => {
        const dummyList = {
            "variable_key_1": {
                "description": "Variable Description",
                "setter": {
                    "nodeId1": {"operation": "=", "value": 1}
                }
            },
            "variable_key_2": {
                "description": "Another Variable Description",
                "setter": {
                    "nodeId3": {"operation": "-", "value": 3},
                    "nodeId4": {"operation": "=", "value": 4},
                }
            }
        };
        const expectedList = {
            "variable_key_2": {
                "description": "Another Variable Description",
                "setter": {
                    "nodeId3": {"operation": "-", "value": 3},
                    "nodeId4": {"operation": "=", "value": 4},
                }
            }
        }
        
        var savedData = {};
        const dummyLoadFile = async (fileName: string) => dummyList
        const dummySaveFile = async (saveSpec: {[fileName: string] : object}) => { savedData = saveSpec["variable.dgconf"]; }

        const variableManager = new VariableManager(
            async() => Promise.resolve(), 
            dummySaveFile, 
            dummyLoadFile
        );
        await variableManager.initialise();
        await variableManager.unsetVariable("variable_key_1", "nodeId1");

        assert.deepStrictEqual(savedData, expectedList);
    });

    it("setVariable should add new setter to existing variables", async () => {
        var savedData = {};
        const dummyLoadFile = async (fileName: string) => dummyVariableList
        const dummySaveFile = async (saveSpec: {[fileName: string] : object}) => { savedData = saveSpec["variable.dgconf"]; }
        const variableManager = new VariableManager(
            async() => Promise.resolve(), 
            dummySaveFile, 
            dummyLoadFile
        );
        await variableManager.initialise();
        await variableManager.setVariable("variable_key_1", "nodeId3", "", DigraphVariableOperation.ADD, 3)

        const expectedVariableList = {
            "variable_key_1": {
                "description": "Variable Description",
                "setter": {
                    "nodeId1": {"operation": "=", "value": 1},
                    "nodeId2": {"operation": "+", "value": 2},
                    "nodeId3": {"operation": "+", "value": 3},
                }
            },
            "variable_key_2": {
                "description": "Another Variable Description",
                "setter": {
                    "nodeId3": {"operation": "-", "value": 3},
                    "nodeId4": {"operation": "=", "value": 4},
                }
            }
        }
        assert.deepStrictEqual(savedData, expectedVariableList);
    });

    it("setVariable should be able to add new variable entry", async () => {
        var savedData = {};
        const dummyLoadFile = async (fileName: string) => dummyVariableList
        const dummySaveFile = async (saveSpec: {[fileName: string] : object}) => { savedData = saveSpec["variable.dgconf"]; }
        const variableManager = new VariableManager(
            async() => Promise.resolve(), 
            dummySaveFile, 
            dummyLoadFile
        );
        await variableManager.initialise();
        await variableManager.setVariable("variable_key_3", "nodeId5", "Newly added variable", DigraphVariableOperation.ADD, 5)

        const expectedVariableList = {
            "variable_key_1": {
                "description": "Variable Description",
                "setter": {
                    "nodeId1": {"operation": "=", "value": 1},
                    "nodeId2": {"operation": "+", "value": 2},
                }
            },
            "variable_key_2": {
                "description": "Another Variable Description",
                "setter": {
                    "nodeId3": {"operation": "-", "value": 3},
                    "nodeId4": {"operation": "=", "value": 4},
                }
            },
            "variable_key_3": {
                "description": "Newly added variable",
                "setter": {
                    "nodeId5": {"operation": "+", "value": 5},
                }
            }
        }
        assert.deepStrictEqual(savedData, expectedVariableList);
    });

});