import { describe } from 'mocha';
import assert from 'assert';
import CodexManager from '../../../src/operation/project/CodexManager';
import { CodexType } from '../../../src/entity/Codex';
import Try from '../../../src/meta/Try';

describe("CodexManager", () => {
    it ("initialise should create files return rejected future if init fails", async () => {
        const dummyHandleInit = async (initSpec: {[key: string]: object}) => {
            return Promise.reject(new Error("init fails"));
        }
        const codexManager = new CodexManager(dummyHandleInit, async () => Try.failure(Error("test stub")))
        assert.rejects( codexManager.initialise() );
    });

    it ("initialise should create the files", async () => {
        var actualInitSpec;
        const dummyHandleInit = async (initSpec: {[key: string]: object}) => {
            actualInitSpec = initSpec;
        }
        const codexManager = new CodexManager(dummyHandleInit, async () => Try.failure(new Error("test stub")))
        await codexManager.initialise();

        const expectedSpec = {
            "codex/characters.dgconf": {},
            "codex/terms.dgconf": {},
            "codex/entities.dgconf": {},
            "codex/items.dgconf": {},
            "codex/locations.dgconf": {},
        }
        
        assert.deepStrictEqual(actualInitSpec, expectedSpec);
    });

    it ("initialise should read all the files and store the result in memory", async () => {
        const dummyHandleLoadFile = async function(filePath: string) {
            return {
                "test": {
                    "names": ["stub"],
                    "description": filePath
                }
            };
        }

        const codexManager = new CodexManager(async () => Promise.resolve(), dummyHandleLoadFile);
        
        await codexManager.initialise();
        const codexData = codexManager.getCodexData();

        const expectedCodex = {
            [CodexType.CHARACTER.toString()] : { test: { names : ["stub"], description: "codex/characters.dgconf" }},
            [CodexType.TERM.toString()] : { test: { names : ["stub"], description: "codex/terms.dgconf" }},
            [CodexType.ENTITY.toString()] : { test: { names : ["stub"], description: "codex/entities.dgconf" }},
            [CodexType.ITEM.toString()] : { test: { names : ["stub"], description: "codex/items.dgconf" }},
            [CodexType.LOCATION.toString()] : { test: { names : ["stub"], description: "codex/locations.dgconf" }},
        }
        
        assert.deepStrictEqual(codexData, expectedCodex);
    });
    
});