import { describe } from 'mocha';
import assert from 'assert';
import ScenarioManager from '../../../src/operation/project/ScenarioManager';
import { DigraphNodeContract } from '../../../src/contract/DigraphNodeContract';
import Try from '../../../src/meta/Try';

describe("ScenarioManager", () => {
    it("initialise should produce barebones cytograph data", async () => {
        var actualInitData;
        const dummyHandleInit = async (initSpec: {[fileName:string] : object}) => {
            actualInitData = initSpec;
        }
        const scenarioManager = new ScenarioManager(
            dummyHandleInit,
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.reject(new Error("test stub")),
            async () => Promise.resolve(),
        );
        await scenarioManager.initialise("initId");
        const expectedInitData = {
            "start.dgjson": [
                {
                    "data": {
                        "id": "initId", 
                        "title": "Start",
                        "digraph": 1,
                        "type": DigraphNodeContract.TYPE_TERMINAL_START
                    }
                }
            ]
        };
            
        assert.deepStrictEqual(actualInitData, expectedInitData);
    });
    
    it("deleteScenarioAsync should return true if file name is not found in the index", async () => {
        const scenarioManager = new ScenarioManager(
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.reject(new Error("not found...")),
            async () => Promise.resolve()
        );
        assert.doesNotReject(scenarioManager.deleteScenarioAsync("targetNode"));
    });
    
    it("deleteScenarioAsync should return false if file system deletion fails", async () => {
        const scenarioManager = new ScenarioManager(
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.reject(new Error("delete failed")),
            async () => Promise.resolve(),
            async () => "gumbo.dgjson",
            async () => Promise.resolve()
        );
        assert.rejects(scenarioManager.deleteScenarioAsync("targetNode"));
    });
    
    it("deleteScenarioAsync should return false if file index deletion fails", async () => {
        const scenarioManager = new ScenarioManager(
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => "gumbo.dgjson",
            async () => Promise.reject(new Error("delete index failed"))
        );
        assert.rejects(scenarioManager.deleteScenarioAsync("targetNode"));
    });

    it("deleteScenarioAsync should return true if file index deletion is successful", async () => {
        const scenarioManager = new ScenarioManager(
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => "gumbo.dgjson",
            async () => Promise.resolve(),
        );
        assert.doesNotReject(scenarioManager.deleteScenarioAsync("targetNode"));
    });

    it("addScenarioAsync should return packed exception if folder creation fails", async () => {
        const scenarioManager = new ScenarioManager(
            async () => Promise.resolve(),
            async () => Promise.reject(new Error("failed to create directory")),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.reject(new Error("test stub")),
            async () => Promise.resolve(),
        );
        assert.rejects( scenarioManager.addNewScenarioAsync("nodeId1", "test/folder/file.dgjson") );
    });

    it("addScenarioAsync should return packed exception if file creation fails", async () => {
        const scenarioManager = new ScenarioManager(
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.reject(new Error("failed to create file")),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.reject(new Error("test stub")),
            async () => Promise.resolve()
        );
        assert.rejects( scenarioManager.addNewScenarioAsync("nodeId1", "test/folder/file.dgjson") );
    });

    it("addScenarioAsync should return packed exception if index registration fails", async () => {
        const scenarioManager = new ScenarioManager(
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.reject(new Error("failed to add to index")),
            async () => Promise.reject(new Error("test stub")),
            async () => Promise.resolve()
        );
        assert.rejects( scenarioManager.addNewScenarioAsync("nodeId1", "test/folder/file.dgjson") );
    });

    it("addScenarioAsync should return return the newly created nodeId", async () => {
        const scenarioManager = new ScenarioManager(
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            async () => Promise.reject(new Error("test stub")),
            async () => Promise.resolve(),
        );
        const newNodeId = await scenarioManager.addNewScenarioAsync("nodeId1", "test/folder/file.dgjson");
        assert.strictEqual(newNodeId.length, 36);
    });
});