import { describe } from 'mocha';
import { assert } from 'chai';
import CharacterManager from '../../../src/operation/project/CharacterManager';

describe("CharacterManager", async () => {
    it("initialise should load character data to memory", async () => {
        const expectedCharData = {
            "Gertrud Rosenkopf": {
                "trigger": "anni",
                "names": ["Gertrud", "Anni"],
                "keywords": ["abrasive", "amoral", "adventurous", "boke", "large ham", "imaginative", "impulsive", "jokester", "outgoing"],
                "description": "Born NK 12234, Gertrud, also known as \"Anni\", is a traveling mage-scholar who extensively explored the world in her lifetime. This book records of her involvement in the fall of Bhumi Dynasty of Aarkjat.",
                "documentation": "The heroine of the story\n### Speech\nRP English (<=> Esperanto)\n### Ticks\nOften interjects with -lah, -ah, -pa due to living for some time in Yun"
            },
            "Kasul": {
                "trigger": "kasul",
                "keywords": ["meticulous", "pragmatic", "risk-averse", "cold ham", "tsukkomi", "xenophobe"],
                "variant": ["sick", "baffled", "confident", "staring"],
                "description": "Born NK 12230, Kasul is Anni's oldest traveling partner, accompanying her since they met during Yun's March Against The Horsed Devils. Like in many other volumes, this volume extensively features his writings and his interpretations of what happened in Aarkjat.",
                "documentation": "The hero of the story\n### Speech\n* RP English (<=> Esperanto)\n* Mandarin Chinese\n* Mongolian\n\n### Lisp\nKasul has a lip scar that makes him hard to spell \"b-\" and \"p-\""
            },
            "Airlangga": {
                "trigger": "airlangga",
                "names": ["Rakai Halu Sri Lokeswara Dharmawangsa Airlangga Anantawikramottunggadewa", "Maharaja"],
                "keywords": ["calm", "resigned", "jolly", "cynical", "social darwinist"],
                "description": "",
                "documentation": "The reigning king of Aarkjat"
            }
        };
        const dummyLoadFile = async (fileName: string) => { return expectedCharData; }
        const characterManager = new CharacterManager(dummyLoadFile);
        
        await characterManager.initialise();

        const characterData = await characterManager.listAllCharacters();
        assert.deepStrictEqual(characterData, expectedCharData);
    });
});