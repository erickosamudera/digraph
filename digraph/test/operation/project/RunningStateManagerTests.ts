import { describe } from 'mocha';
import { assert } from 'chai';
import RunningStateManager from '../../../src/operation/project/RunningStateManager';
import { InitSpec } from '../../../src/entity/Project';
import { RunningStateKey, RunningState } from '../../../src/entity/RunningState';

describe("RunningStateManager", () => {
    it("initialise should write empty object to running state file", async () => {
        var actualInitSpec;
        const handleInit = async (initSpec: InitSpec) => {
            actualInitSpec = initSpec;
        };
        const runningStateManager = new RunningStateManager(
            handleInit,
            async () => Promise.resolve(),
            async () => { return {}; }
        );
        await runningStateManager.initialise();
        assert.deepStrictEqual(actualInitSpec, { "state.dgconf" : {}});
    });
    
    it("updateLastReadFile should write last read file name to config", async () => {
        var actualSaveSpec = {};
        const dummySaveFileHandler = async (saveSpec: {[key: string]: object}) : Promise<void> => {
            actualSaveSpec = saveSpec;
        }
        const runningStateManager = new RunningStateManager(
            async () => Promise.resolve(),
            dummySaveFileHandler,
            async () => { return {}; }
        );
        await runningStateManager.updateLastReadFile("ch1/arrival.dgjson");
        assert.deepStrictEqual(actualSaveSpec, { "state.dgconf" : { [RunningStateKey.LAST_READ_FILE] : "ch1/arrival.dgjson"} });
    });

    it("getLastReadFile should return last read file saved in the file", async () => {
        const dummyLoadHandler = async (filePath: string) : Promise<RunningState> => {
            return {
                [RunningStateKey.LAST_READ_FILE] : "ch1/arrival.dgjson"
            };
        }
        const runningStateManager = new RunningStateManager(
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            dummyLoadHandler
        );
        const lastReadFile = await runningStateManager.getLastReadFile();
        assert.strictEqual(lastReadFile, "ch1/arrival.dgjson");
    });

    it("getLastReadFile should return start file if key is not found", async () => {
        const dummyLoadHandler = async (filePath: string) : Promise<RunningState> => {
            return {};
        }
        const runningStateManager = new RunningStateManager(
            async () => Promise.resolve(),
            async () => Promise.resolve(),
            dummyLoadHandler
        );
        const lastReadFile = await runningStateManager.getLastReadFile();
        assert.strictEqual(lastReadFile, "start.dgjson");
    });
});