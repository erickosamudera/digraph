import { describe } from 'mocha';
import assert from 'assert';
import FileIndexManager from '../../../src/operation/project/FileIndexManager';

describe("FileIndexManager", () => {
    it("getFileName should return file name of a connector", async () => {
        const handleLoadFile = async (ignored: string) => {
            return {
                "conn_id_1": {name: "file_1", connectedTo: []},
                "conn_id_2": {name: "file_2", connectedTo: []},
                "conn_id_3": {name: "file_3", connectedTo: []},
            }
        }

        const fileIndexManager = new FileIndexManager(
            async (initSpec) => Promise.resolve(), 
            async(data) => Promise.resolve(), 
            handleLoadFile
        );
        assert.strictEqual(await fileIndexManager.getFileName("conn_id_1"), "file_1");
        assert.strictEqual(await fileIndexManager.getFileName("conn_id_2"), "file_2");
        assert.strictEqual(await fileIndexManager.getFileName("conn_id_3"), "file_3");
    });
    
    it("getFileName should return exception if file is not registered", async () => {
        const handleLoadFile = async (ignored: string) => {
            return {
                "conn_id_1": {name: "file_1", connectedTo: []},
                "conn_id_2": {name: "file_2", connectedTo: []},
                "conn_id_3": {name: "file_3", connectedTo: []},
            }
        }
        const fileIndexManager = new FileIndexManager(
            async (initSpec) => Promise.resolve(), 
            async(data) => Promise.resolve(), 
            handleLoadFile
        );
        assert.rejects(fileIndexManager.getFileName("conn_id_4"));
    });
    
    it("deleteFromIndex should remove selected file index", async () => {
        const handleLoadFile = async (ignored: string) => {
            return {
                "conn_id_1": {name: "file_1", connectedTo: []},
                "conn_id_2": {name: "file_2", connectedTo: []},
                "conn_id_3": {name: "file_3", connectedTo: []},
            }
        };
        var savedData;
        const handleSaveFile = async (jsonData: object) => {
            savedData = jsonData;
        };
        const fileIndexManager = new FileIndexManager(async (initSpec) => Promise.resolve(), handleSaveFile, handleLoadFile);
        await fileIndexManager.deleteFromIndex("conn_id_2");
        const expected = {
            "fileIndex.dgconf": {
                "conn_id_1": {name: "file_1", connectedTo: []},
                "conn_id_3": {name: "file_3", connectedTo: []},
            }
        }
        assert.deepStrictEqual(savedData, expected);
    });
    
    it("registerNewFileToIndex should add new index and update node connection", async () => {
        const handleLoadFile = async (ignored: string) => {
            return {
                "conn_id_1": {name: "file_1", connectedTo: []},
                "conn_id_2": {name: "file_2", connectedTo: []},
                "conn_id_3": {name: "file_3", connectedTo: []},
            }
        };
        var savedData;
        const handleSaveFile = async (jsonData: object) => {
            savedData = jsonData;
        };
        const fileIndexManager = new FileIndexManager(async (initSpec) => Promise.resolve(), handleSaveFile, handleLoadFile);
        await fileIndexManager.registerNewFileToIndex("conn_id_1", "conn_id_4", "file_4")
        const expected = {
            "fileIndex.dgconf": {
                "conn_id_1": {name: "file_1", connectedTo: ["conn_id_4"]},
                "conn_id_2": {name: "file_2", connectedTo: []},
                "conn_id_3": {name: "file_3", connectedTo: []},
                "conn_id_4": {name: "file_4", connectedTo: []},
            }
        }
        assert.deepStrictEqual(savedData, expected);
    });
})