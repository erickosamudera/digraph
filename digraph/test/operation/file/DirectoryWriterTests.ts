import { describe } from 'mocha';
import { assert } from 'chai';
import fs from 'fs';
import DirectoryWriter from '../../../src/operation/file/DirectoryWriter';

describe("DirectoryWriter", () => {
    it("createDirectoryAsync should not be rejected if making directory is successful", async () => {
        const tmpDir = '/tmp/a/b/';

        await new DirectoryWriter(tmpDir).createDirectoryAsync()
        assert.isTrue(fs.existsSync(tmpDir))
        
        fs.rmdirSync('/tmp/a/b/');
        fs.rmdirSync('/tmp/a/');
    });

    it("createDirectoryAsync should not be rejected if directory already exists", async () => {
        const tmpDir = '/tmp/a/b/'

        fs.mkdirSync(tmpDir, { recursive: true });
        await new DirectoryWriter(tmpDir).createDirectoryAsync()
        assert.isTrue(fs.existsSync(tmpDir))
        
        fs.rmdirSync('/tmp/a/b/');
        fs.rmdirSync('/tmp/a/');
    });
});