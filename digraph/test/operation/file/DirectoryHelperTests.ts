import { describe } from 'mocha';
import { assert } from 'chai';
import DirectoryHelper from '../../../src/operation/file/DirectoryHelper';

describe("DirectoryHelper", () => {
    it("getDirectoryOf should return the file's directory", () => {
        const directory = DirectoryHelper.getDirectoryOf("/tmp/desudesu/asd.dgjson")
        assert.strictEqual(directory, "/tmp/desudesu/");
    });
});