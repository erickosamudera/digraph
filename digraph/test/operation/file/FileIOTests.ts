import { describe } from 'mocha';
import assert from 'assert';
import uuidv1 from 'uuid/v1';
import { writeJsonAsync, readJsonAsync } from '../../../src/operation/file/FileIO';
import fs from 'fs';

describe("FileIO", () => {
    it("writeJSONAsync should not reject if write successful", async () => {
        const fileId = uuidv1();
        const tmpFile = '/tmp/' + fileId;
        const jsonObject = {"data": 123}

        await writeJsonAsync(tmpFile, jsonObject);

        const writtenString = fs.readFileSync(tmpFile).toString();
        assert.strictEqual(writtenString, "{\"data\":123}")
        fs.unlinkSync(tmpFile);
    });

    it("writeJSONAsync should be rejected if write unsuccessful", async () => {
        const jsonObject = {"data": 123}
        assert.rejects(writeJsonAsync("/tmp/imaginaryfolder/doesntexistfile", jsonObject));
    });
    
    it("readJsonAsync should read JSON file and parse it to object", async () => {
        const fileId = uuidv1();
        const tmpFile = '/tmp/' + fileId;

        fs.writeFileSync(tmpFile, "{\"desu\":[1, 2, 3]}");

        const jsonData = await readJsonAsync(tmpFile);
        assert.strictEqual(2, jsonData.desu[1]);

        fs.unlinkSync(tmpFile);
    });
    
    it("readJsonAsync should return error if JSON parsing fails", async () => {
        const fileId = uuidv1();
        const tmpFile = '/tmp/' + fileId;
        
        fs.writeFileSync(tmpFile, "{\"desu\":[1, 2");

        assert.rejects(readJsonAsync(tmpFile));
        
        fs.unlinkSync(tmpFile);
    });
    
    it("readJsonAsync should throw exception if reader fails", async () => {
        const fileId = uuidv1();
        const tmpFile = '/tmp/' + fileId;

        assert.rejects(readJsonAsync(tmpFile));
    });
});