import { describe } from 'mocha';
import assert from 'assert';
import fs from 'fs';
import uuidv1 from 'uuid/v4';
import FileDeleter from '../../../src/operation/file/FileDeleter';

describe("File Deleter", () => {
    it("deleteAsync should return true if deletion successful", async () => {
        const fileId = uuidv1();
        const tmpFile = '/tmp/' + fileId;

        fs.writeFileSync(tmpFile, "{\"desu\":[1, 2, 3]}");

        assert.doesNotReject(new FileDeleter(tmpFile).deleteAsync());
        assert.strictEqual(fs.existsSync(tmpFile), true);
    });
    
    it("deleteAsync should return true if target file doesnt exist", async () => {
        const tmpFile = '/tmp/imaginaryfolder/bogusfile';
        assert.doesNotReject(new FileDeleter(tmpFile).deleteAsync());
    });
});