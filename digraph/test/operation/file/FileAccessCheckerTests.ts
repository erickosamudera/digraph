import { describe } from 'mocha';
import { assert, expect } from 'chai';
import fs from 'fs';
import uuidv1 from 'uuid/v4';
import FileAccessChecker from '../../../src/operation/file/FileAccessChecker';

describe("FileAccessChecker", () => {
    it("checkAsync should return true if file exists", async () => {
        const fileId = uuidv1();
        const tmpFile = '/tmp/' + fileId;

        fs.writeFileSync(tmpFile, "{\"desu\":[1, 2, 3]}");

        const isExist = await new FileAccessChecker(tmpFile).checkAsync();
        assert.isTrue(isExist);

        fs.unlinkSync(tmpFile);
    });

    it("checkAsync should return false if file doesnt exist", async () => {
        const fileId = uuidv1();
        const tmpFile = '/tmp/' + fileId;

        const isExist = await new FileAccessChecker(tmpFile).checkAsync();
        assert.isFalse(isExist);
    });
});