import {describe} from 'mocha';
import assert from 'assert';
import {DigraphVariableOperation} from "../../../src/entity/DigraphVariable";
import {getCharacterSuggestion, getDigraphVariable} from "../../../src/operation/suggestion/ProduceSuggestion";
import {DigraphSuggestionContract} from "../../../src/contract/DigraphSuggestionContract";

describe("OutputProduction", () => {
    it("getDigraphVariable should list all listed variables", () => {
        const variableList = {
            "variable1": {
                description: "description about variable 1",
                setter: {"nodeId1": {operation: DigraphVariableOperation.ADD, value: 1}}
            },
            "variable2": {
                description: "description about variable 2",
                setter: {"nodeId2": {operation: DigraphVariableOperation.ASSIGN, value: 5}}
            }
        };

        const suggestionList = getDigraphVariable(variableList);
        const expectedSuggestionList = [
            {
                label: "variable1",
                kind: DigraphSuggestionContract.Variable,
                insertText: "variable1 ",
            },
            {
                label: "variable2",
                kind: DigraphSuggestionContract.Variable,
                insertText: "variable2 ",
            },
        ];

        assert.deepStrictEqual(suggestionList, expectedSuggestionList);
    });

    it("getCharacterSuggestion should create SuggestionList from the character list", () => {
        const characterList = {
            "Budi Suhendra": {
                names: ["Di", "Hendra"],
                keywords: ["trait1", "trait2", "trait3"],
                trigger: "budi",
                documentation: "writer documentation on Budi"
            },
            "Shinta Kertosastro": {
                names: ["Shin-shin", "Sastro"],
                keywords: ["trait4", "trait5", "trait6"],
                trigger: "shinta",
                documentation: "writer documentation on Shinta"
            }
        };

        const dummyHandleDocumentation = (entryId: string, otherNames: Array<string>, description: string) => {
            return "additional stuffs and " + description
        };

        const expectedSuggestionList = [
            {
                label: "budi",
                detail: "person",
                kind: DigraphSuggestionContract.Class,
                documentation: { value: "additional stuffs and writer documentation on Budi" },
                insertText: "budi bang!",
                insertTextRules: 4
            },
            {
                label: "shinta",
                detail: "person",
                kind: DigraphSuggestionContract.Class,
                documentation: { value: "additional stuffs and writer documentation on Shinta" },
                insertText: "shinta bang!",
                insertTextRules: 4
            }
        ];

        const suggestionList = getCharacterSuggestion(
            characterList, dummyHandleDocumentation, (trigger) => { return trigger + " bang!"});
        assert.deepStrictEqual(suggestionList, expectedSuggestionList);
    });

    it("getCodexEntry should create SuggestionList from the codex entries", () => {
        // const codex = {
        //     [CodexType.LOCATION]: {
        //         "Jakarta": {names: ["Batavia", "Sunda Kelapa"], description: "Ibukota RI"},
        //         "Surabaya": {names: [], description: "Ibukota Jawa Timur"},
        //         "Papua": {names: ["Irian Jaya"], description: "Pulau paling timur RI"},
        //     },
        //     [CodexType.ENTITY]: {
        //         "VOC": {names: ["Kumpeni"], description: "Serikat dagang dari Belanda"},
        //     }
        // };
        //
        // const dummyHandleDocumentation = (entryId: string, otherNames: Array<string>, description: string) => {
        //     return "additional stuffs and " + description
        // };
        //
        // const expectedSuggestionList = [
        //     {
        //         label: "Jakarta",
        //         detail: CodexType.LOCATION,
        //         kind: DigraphSuggestionContract.Value,
        //         documentation: { value: "additional stuffs and Ibukota RI" },
        //         insertText: "Jakarta",
        //     },
        //     {
        //         label: "Batavia",
        //         detail: CodexType.LOCATION,
        //         kind: DigraphSuggestionContract.Value,
        //         documentation: { value: "additional stuffs and Ibukota RI" },
        //         insertText: "Batavia",
        //     },
        //     {
        //         label: "Sunda Kelapa",
        //         detail: CodexType.LOCATION,
        //         kind: DigraphSuggestionContract.Value,
        //         documentation: { value: "additional stuffs and Ibukota RI" },
        //         insertText: "Sunda Kelapa",
        //     },
        //     {
        //         label: "Surabaya",
        //         detail: CodexType.LOCATION,
        //         kind: DigraphSuggestionContract.Value,
        //         documentation: { value: "additional stuffs and Ibukota Jawa Timur" },
        //         insertText: "Surabaya",
        //     },
        //     {
        //         label: "Papua",
        //         detail: CodexType.LOCATION,
        //         kind: DigraphSuggestionContract.Value,
        //         documentation: { value: "additional stuffs and Pulau paling timur RI" },
        //         insertText: "Papua",
        //     },
        //     {
        //         label: "Irian Jaya",
        //         detail: CodexType.LOCATION,
        //         kind: DigraphSuggestionContract.Value,
        //         documentation: { value: "additional stuffs and Pulau paling timur RI" },
        //         insertText: "Irian Jaya",
        //     },
        //     {
        //         label: "VOC",
        //         detail: CodexType.ENTITY,
        //         kind: DigraphSuggestionContract.Value,
        //         documentation: { value: "additional stuffs and Serikat dagang dari Belanda" },
        //         insertText: "VOC",
        //     },
        //     {
        //         label: "Kumpeni",
        //         detail: CodexType.ENTITY,
        //         kind: DigraphSuggestionContract.Value,
        //         documentation: { value: "additional stuffs and Serikat dagang dari Belanda" },
        //         insertText: "Kumpeni",
        //     }
        // ];
        //
        // const suggestionList = getCodexEntry(codex, dummyHandleDocumentation);
        // assert.deepStrictEqual(suggestionList, expectedSuggestionList);
    });
});
