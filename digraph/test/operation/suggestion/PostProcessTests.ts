import { describe } from 'mocha';
import assert from 'assert';
import {attachTriggerDeleter} from "../../../src/operation/suggestion/PostProcess";


describe("PostProcess", () => {
    it("attachTriggerDeleter should add additional text edit to delete trigger char", () => {
        const dummySuggestionList = [
            {
                label: "whatever",
                kind: "doesnt matter",
                insertText: "tralala",
            }
        ];
        const dummyPosition = {
            lineNumber: 10,
            column: 12
        };

        //@ts-ignore: can't import monaco in tests, so we just mock all the relevant bits from Position object
        const modifiedSuggestion = attachTriggerDeleter(dummySuggestionList, dummyPosition);

        const expectedSuggestionList = [
            {
                label: "whatever",
                kind: "doesnt matter",
                insertText: "tralala",
                additionalTextEdits: [
                    {
                        forceMoveMarkers: false,
                        range: {
                            startColumn: 11,
                            endColumn: 12,
                            startLineNumber: 10,
                            endLineNumber: 10
                        },
                        text: ""
                    }
                ]
            }
        ];

        assert.deepStrictEqual(modifiedSuggestion, expectedSuggestionList);
    });
});