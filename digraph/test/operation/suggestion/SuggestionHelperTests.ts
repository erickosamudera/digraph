import { describe } from 'mocha';
import { assert } from 'chai';
import SuggestionHelpers from '../../../src/operation/suggestion/SuggestionHelpers';

describe("SuggestionHelpers", () => {
    it("handleCreateDocumentation should create doc from inputted parameters", () => {
        const documentation = SuggestionHelpers.handleCreateDocumentation(
            "test",
            ["coba", "dummy"],
            "deskripsi"
        )
        const expectedDoc = "# test\nOther names:\n* coba\n* dummy\n\ndeskripsi";
        assert.strictEqual(documentation, expectedDoc);
    });

});